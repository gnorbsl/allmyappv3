package de.allmystery.patric.allmyapp.activities;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import com.ToxicBakery.viewpager.transforms.ZoomOutSlideTransformer;
import com.crashlytics.android.Crashlytics;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.adapter.CategoryPagerAdapter;
import de.allmystery.patric.allmyapp.adapter.PostListAdapter;
import de.allmystery.patric.allmyapp.events.BookmarkEvent;
import de.allmystery.patric.allmyapp.events.LogoutEvent;
import de.allmystery.patric.allmyapp.events.NewUserDataEvent;
import de.allmystery.patric.allmyapp.events.NotificationEvent;
import de.allmystery.patric.allmyapp.events.PnEvent;
import de.allmystery.patric.allmyapp.events.ShowConversationEvent;
import de.allmystery.patric.allmyapp.events.ShowLastEntryEvent;
import de.allmystery.patric.allmyapp.events.UpdateUIEvent;
import de.allmystery.patric.allmyapp.fragments.CategoryFragment;
import de.allmystery.patric.allmyapp.fragments.PageHandlerFragment;
import de.allmystery.patric.allmyapp.fragments.ThreadFragment;
import de.allmystery.patric.allmyapp.helper.ActivityAnimator;
import de.allmystery.patric.allmyapp.helper.Analytics;
import de.allmystery.patric.allmyapp.helper.Category;
import de.allmystery.patric.allmyapp.helper.ThreadObject;
import de.allmystery.patric.allmyapp.helper.Tools;
import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by Patric on 26.05.2014.
 */
public class CategoryActivity extends ActionBarActivity
        implements PageHandlerFragment.ClickListener, CategoryFragment.ProgressInterface,
        ThreadFragment.ProgressInterface, PostListAdapter.ListScrollListener {


    static SmoothProgressBar progressBar;

    TextView threadTitle;

    ViewPager pager;

    PageHandlerFragment frag;

    Category category;

    ThreadObject thread;

    String cat;

    int pages;

    boolean isClosed;

    Bundle extra;

    MenuItem pnItem;

    MenuItem bookmarkItem;

    View pnActionView;

    View bookActionView;

    TextView badgeViewPn = null;

    TextView badgeViewBookmark = null;


    private PnEvent pnEvent;

    private BookmarkEvent bookmarkEvent;

    private Menu menu;

    public void showProgressBar(boolean showProgressBar) {

        if (showProgressBar) {
            progressBar.setVisibility(View.VISIBLE);

        } else {
            progressBar.setVisibility(View.INVISIBLE);

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        Crashlytics.start(this);

        final SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
        Tools.getInstance(CategoryActivity.this);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().registerSticky(this);
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressBar = (SmoothProgressBar) findViewById(R.id.progress);

        pager = (ViewPager) findViewById(R.id.viewPagerCategory);

        frag = (PageHandlerFragment)
                getSupportFragmentManager().findFragmentById(R.id.pageHandlerFragment);

        extra = getIntent().getExtras();

        if (extra != null) {

            if (extra.containsKey("de.allmystery.patric.allmyapp.category")) {

                Analytics.tagScreen(getApplicationContext(), "CategoryActivity Category");

                category = extra.getParcelable("de.allmystery.patric.allmyapp.category");

                cat = category.getCat();

                getSupportActionBar().setTitle(category.getTitle_short());

                pages = (int) Math.ceil(category.getThreads_count() / 25.0);

                try {
                    pager.setAdapter(new CategoryPagerAdapter(
                            CategoryActivity.this.getSupportFragmentManager(), pages,
                            category));
                } catch (IllegalStateException e) {
                    Crashlytics.logException(e);
                }

                handlePageNavigation(pages);

            } else if (extra.containsKey("de.allmystery.patric.allmyapp.thread")) {

                Analytics.tagScreen(getApplicationContext(), "CategoryActivity Thread");

                thread = extra.getParcelable("de.allmystery.patric.allmyapp.thread");

                showThread(thread);

            }

        }

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        final LinearLayout adLayout = (LinearLayout) findViewById(R.id.adlayout);
        final AdView mAdView = (AdView) findViewById(R.id.adView);
        if (sharedPref.contains("showads") && sharedPref.getBoolean("showads", true)) {

            AdRequest.Builder builder = new AdRequest.Builder();
            builder.addTestDevice("A76892F27DEDD695B0D9AD2F7440BF71");
            builder.addKeyword(category != null ? category.getTitle_short()
                    : thread != null ? thread.getTitle() : "");

            AdRequest adRequest = builder.build();

            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);
                    adLayout.setVisibility(View.GONE);
                }

            });

            mAdView.loadAd(adRequest);

        } else {
            adLayout.setVisibility(View.GONE);
        }

        if (extra != null) {

            if ("SHOW THREAD".equals(getIntent().getAction())) {

                final String threadId = extra.getString("de.allmystery.patric.allmyapp.newThread");

                if (threadId != null) {

                    Tools.startThread(Integer.parseInt(threadId), CategoryActivity.this,
                            new Intent(CategoryActivity.this, CategoryActivity.class)
                                    .putExtra("de.allmystery.patric.allmyapp.thread.showLastPage",
                                            true)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                    NotificationManager mNotificationManager = (NotificationManager)
                            this.getSystemService(Context.NOTIFICATION_SERVICE);

                    mNotificationManager.cancel(1);
                    Tools.clearNotifications();

                }

            }

        }


    }

    private void showThread(ThreadObject thread) {

        cat = thread.getCategory();

        SharedPreferences threadData = getSharedPreferences(
                String.valueOf(thread.getThread_id()), MODE_PRIVATE);

        pages = thread.getPages();
        isClosed = thread.isClosed();
        getSupportActionBar().setTitle("");

        threadTitle = (TextView) findViewById(R.id.threadTitle);
        threadTitle.setText(thread.getTitle());
        threadTitle.setVisibility(View.VISIBLE);

        try {
            pager.setAdapter(new CategoryPagerAdapter(
                    CategoryActivity.this.getSupportFragmentManager(), pages,
                    thread));
        } catch (IllegalStateException e) {
            Crashlytics.logException(e);
        }

        handlePageNavigation(pages);

        if (extra.containsKey("de.allmystery.patric.allmyapp.thread.showLastPage")) {

            pager.setCurrentItem(pages);
            frag.changePosition(pages);

        } else if (threadData.contains("pageNumber")) {

            pager.setCurrentItem(threadData.getInt("pageNumber", -1) - 1);

        }

    }


    private void handlePageNavigation(int pages) {

        if (pages == 0) {
            pages = 1;
        }

        frag.handleNavigation(pager.getCurrentItem(), pages);

        if (Tools.getOrientation() == 1 || extra.containsKey("threadId")) {

            pager.setPageTransformer(true, new ZoomOutSlideTransformer());
        }

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                    int positionOffsetPixels) {
                frag.changePosition((position + 1), positionOffset);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        if (extra.containsKey("jumpTo")) {
            frag.changePosition(extra.getInt("jumpTo"));
            pager.setCurrentItem(extra.getInt("jumpTo"));
        }

    }


    public String getCategoryString() {

        if (cat != null && !cat.equals("")) {
            return cat;
        } else {
            return category.getCat();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        this.menu = menu;

        if (!extra.containsKey("de.allmystery.patric.allmyapp.thread") && !extra
                .containsKey("threadId")) {

            MenuItem refresh = this.menu.findItem(R.id.action_refreshcat);
            refresh.setVisible(true);
        }

        pnItem = this.menu.findItem(R.id.action_newPn);
        bookmarkItem = this.menu.findItem(R.id.action_newBookmark);

        MenuItemCompat.setActionView(pnItem, R.layout.notification_iconpn);
        MenuItemCompat.setActionView(bookmarkItem, R.layout.notification_iconbook);

        pnActionView = MenuItemCompat.getActionView(pnItem);
        bookActionView = MenuItemCompat.getActionView(bookmarkItem);

        badgeViewPn = (TextView) pnActionView.findViewById(R.id.badge_textview);
        badgeViewBookmark = (TextView) bookActionView.findViewById(R.id.badge_textview);

        //Add clicklistener to Actionbaritems
        pnActionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new ShowConversationEvent());

                Intent mainIntent = new Intent(CategoryActivity.this, MainActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(mainIntent);
                new ActivityAnimator().fadeAnimation(CategoryActivity.this);

                finish();
            }
        });

        bookActionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Tools.showBookmarks(CategoryActivity.this);

                finish();

            }
        });

        NewUserDataEvent event = EventBus.getDefault().getStickyEvent(NewUserDataEvent.class);
        handleEvents(event);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                if (((CategoryPagerAdapter) pager.getAdapter())
                        .getRegisteredFragment(pager.getCurrentItem()) instanceof ThreadFragment) {

                    ThreadFragment thread = (ThreadFragment) ((CategoryPagerAdapter) pager
                            .getAdapter())
                            .getRegisteredFragment(pager.getCurrentItem());

                    thread.safeLastPosition();
                }

                Intent intent = new Intent(CategoryActivity.this, MainActivity.class);
                startActivity(intent);
                new ActivityAnimator().fadeAnimation(CategoryActivity.this);
                finish();
                return true;

            case R.id.action_answer:

                if (isClosed) {

                    Crouton.makeText(CategoryActivity.this, "Thread ist geschlossen!", Style.INFO)
                            .show();

                } else {

                    Intent answerIntent = new Intent(CategoryActivity.this, AnswerActivity.class);
                    answerIntent.putExtra("de.allmystery.patric.allmyapp.thread",
                            extra.getParcelable("de.allmystery.patric.allmyapp.thread"));

                    startActivity(answerIntent);
                    new ActivityAnimator().fadeAnimation(CategoryActivity.this);

                }
                break;

            case R.id.action_newBookmark:

                Tools.showBookmarks(CategoryActivity.this);

                finish();
                break;

            case R.id.action_newPn:

                EventBus.getDefault().post(new ShowConversationEvent());

                Intent mainIntent = new Intent(CategoryActivity.this, MainActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(mainIntent);
                new ActivityAnimator().fadeAnimation(CategoryActivity.this);

                break;

            case R.id.action_refreshcat:

                reloadAdapter();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (((CategoryPagerAdapter) pager.getAdapter())
                .getRegisteredFragment(pager.getCurrentItem()) instanceof ThreadFragment) {

            ThreadFragment thread = (ThreadFragment) ((CategoryPagerAdapter) pager.getAdapter())
                    .getRegisteredFragment(pager.getCurrentItem());

            thread.safeLastPosition();
        }

        super.onBackPressed();
    }

    public void reloadAdapter() {

        if (pager != null && pager.getAdapter() != null) {
            pager.getAdapter().notifyDataSetChanged();
        }

    }

    public void onEventMainThread(UpdateUIEvent e) {

        if (e != null) {

            reloadAdapter();
            EventBus.getDefault()
                    .postSticky(new ShowLastEntryEvent(pager.getAdapter().getCount() - 1));
            pager.setCurrentItem(pager.getAdapter().getCount() - 1);

        }

    }

    public void onEventMainThread(NewUserDataEvent e) {

        handleEvents(e);
    }

    public void onEventMainThread(NotificationEvent e) {

        Tools.getUserData(CategoryActivity.this);

    }

    public void onEventMainThread(LogoutEvent e) {

        finish();

    }


    private void handleEvents(NewUserDataEvent e) {

        if (e != null && menu != null) {

            if (e.getNewMessage() > 0) {
                pnItem.setVisible(true);
                if (badgeViewPn != null) {
                    badgeViewPn.setText(e.getNewMessage() + "");
                }

            } else {
                pnItem.setVisible(false);
                pnActionView.setVisibility(View.INVISIBLE);
            }

            if (e.getNewBookmark() > 0) {
                bookmarkItem.setVisible(true);
                if (badgeViewBookmark != null) {
                    badgeViewBookmark.setText(e.getNewBookmark() + "");
                }

            } else {
                bookmarkItem.setVisible(false);
                bookActionView.setVisibility(View.INVISIBLE);
            }

        }

    }


    @Override
    protected void onDestroy() {

        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    //called when item in the PageHandlerFragment is clicked
    @Override
    public void onNavClick(int position) {

        pager.setCurrentItem((position - 1));

    }

    @Override
    protected void onNewIntent(Intent intent) {

        if ("SHOW THREAD".equals(getIntent().getAction())) {

            final String threadId = extra.getString("de.allmystery.patric.allmyapp.newThread");

            if (threadId != null) {

                Tools.startThread(Integer.parseInt(threadId), CategoryActivity.this,
                        new Intent(CategoryActivity.this, CategoryActivity.class)
                                .putExtra("de.allmystery.patric.allmyapp.thread.showLastPage", true)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                NotificationManager mNotificationManager = (NotificationManager)
                        this.getSystemService(Context.NOTIFICATION_SERVICE);

                mNotificationManager.cancel(1);
                Tools.clearNotifications();
            }

        }

    }

    @Override
    public void showTitle() {
        getSupportActionBar().show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Tools.setIsVisible(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Tools.setIsVisible(true);
    }
}
