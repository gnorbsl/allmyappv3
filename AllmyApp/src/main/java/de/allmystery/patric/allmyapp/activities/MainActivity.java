package de.allmystery.patric.allmyapp.activities;


import com.google.android.gms.gcm.GoogleCloudMessaging;

import com.astuetz.PagerSlidingTabStrip;
import com.crashlytics.android.Crashlytics;

import net.simonvt.menudrawer.MenuDrawer;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.adapter.MainActivityMenuAdapter;
import de.allmystery.patric.allmyapp.adapter.MainPagerAdapter;
import de.allmystery.patric.allmyapp.events.LogoutEvent;
import de.allmystery.patric.allmyapp.events.NewUserDataEvent;
import de.allmystery.patric.allmyapp.events.NotificationEvent;
import de.allmystery.patric.allmyapp.events.ShowConversationEvent;
import de.allmystery.patric.allmyapp.fragments.CategoryFragment;
import de.allmystery.patric.allmyapp.fragments.ConversationFragment;
import de.allmystery.patric.allmyapp.fragments.FriendListFragment;
import de.allmystery.patric.allmyapp.fragments.MainFragment;
import de.allmystery.patric.allmyapp.helper.AllmyApiClient;
import de.allmystery.patric.allmyapp.helper.Analytics;
import de.allmystery.patric.allmyapp.helper.Category;
import de.allmystery.patric.allmyapp.helper.MainResponse;
import de.allmystery.patric.allmyapp.helper.Tools;
import de.cketti.library.changelog.ChangeLog;
import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends ActionBarActivity implements MainFragment.ProgressInterface,
        CategoryFragment.ProgressInterface, FriendListFragment.ProgressInterface,
        ConversationFragment.ProgressInterface {



    private static SmoothProgressBar progressBar;

    Menu menu;

    MenuItem pnItem;

    MenuItem bookmarkItem;

    View pnActionView;

    View bookActionView;

    TextView badgeViewPn = null;

    TextView badgeViewBookmark = null;

    GoogleCloudMessaging gcm;

    Context context;

    private ViewPager pager;

    private MenuDrawer drawer;

    private PagerSlidingTabStrip tabs;

    private int pagerOffsetPixels;

    private int pagerPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Crashlytics.start(MainActivity.this);

        Analytics.tagScreen(getApplicationContext(), "MainActivity");

        EventBus.getDefault().registerSticky(MainActivity.this);
        PreferenceManager.setDefaultValues(this, R.xml.pref, false);

        context = getApplicationContext();

        Tools.setActivity(MainActivity.this);
        Tools.registerGCM(MainActivity.this);



        ChangeLog cl = new ChangeLog(this);
        if (cl.isFirstRun()) {
            SharedPreferences sharedPref = PreferenceManager
                    .getDefaultSharedPreferences(MainActivity.this);

            cl.getLogDialog().show();
        }

        AllmyApiClient.getAllmyApiInterface(getApplicationContext()).listCategories("", "desc", new Callback<MainResponse>() {
            @Override
            public void success(MainResponse mainResponse, Response response) {


                List<Category> categories = mainResponse.getCategories();

                for (Category category : categories) {

                    SharedPreferences cats = getSharedPreferences(category.getCat(), Context.MODE_PRIVATE);

                    SharedPreferences.Editor edit = cats.edit();

                    edit.putString("title_short", category.getTitle_short());

                    edit.apply();

                }

            }

            @Override
            public void failure(RetrofitError error) {

                Tools.handleError(MainActivity.this, error);

            }
        });


        //menudrawer doesnt support 3< versions
        if (!(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB)) {
            createMenuDrawer();
        } else {
            setContentView(R.layout.activity_main);
        }

        progressBar = (SmoothProgressBar) findViewById(R.id.progress);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        pager = (ViewPager) findViewById(R.id.viewPagerMain);
        pager.setAdapter(new MainPagerAdapter(getSupportFragmentManager()));

        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(pager);

        tabs.setIndicatorColor(Color.parseColor("#CC5A5C75"));
        tabs.setBackgroundColor(Color.parseColor("#CC404040"));
        tabs.setDividerColor(Color.BLACK);

        tabs.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                    int positionOffsetPixels) {

                pagerPosition = position;
                pagerOffsetPixels = positionOffsetPixels;

            }

        });

        if (getIntent().getAction() != null) {

            Tools.clearNotifications();

            NotificationManager mNotificationManager = (NotificationManager)
                    this.getSystemService(Context.NOTIFICATION_SERVICE);

            mNotificationManager.cancel(1);

            if ("OVERVIEW PN".equals(getIntent().getAction())) {

                pager.setCurrentItem(3);

            } else if ("OVERVIEW BOOKMARKS".equals(getIntent().getAction())) {

                Tools.showBookmarks(MainActivity.this);

            }

        } else {
            Tools.getUserData(MainActivity.this);
        }



    }

    private void createMenuDrawer() {

        drawer = MenuDrawer.attach(this);
        drawer.setContentView(R.layout.activity_main);
        drawer.setSlideDrawable(R.drawable.ic_action_drawer);
        drawer.setDrawerIndicatorEnabled(true);
        drawer.setTouchMode(MenuDrawer.TOUCH_MODE_BEZEL);

        final List<String> items = new ArrayList<>();

        items.add("Navigation");
        items.add("Kategorien");
        items.add("Beobachtet");
        items.add("Freundesliste");
        items.add("Nachrichten");
        items.add("Mehr");
        items.add("Einstellungen");

        MainActivityMenuAdapter menuAdapter = new MainActivityMenuAdapter(this, items);
        ListView list = new ListView(this);

        list.setAdapter(menuAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Analytics.tagEvent(getApplicationContext(), "Seitennavigation benutzt");
                if (items.get(i).equals("Einstellungen")) {

                    startActivity(new Intent(MainActivity.this, SettingsActivity2.class));

                    //jump to desired tab
                } else if (!items.get(i).equals("Navigation")) {

                    drawer.closeMenu();
                    pager.setCurrentItem(i - 1);

                }

            }
        });

        drawer.setMenuView(list);

        drawer.setOnInterceptMoveEventListener(new MenuDrawer.OnInterceptMoveEventListener() {
            @Override
            public boolean isViewDraggable(View v, int dx, int x, int y) {

                //exclude dragging on tabs
                if (v != tabs) {

                    return !(pagerPosition == 0 && pagerOffsetPixels == 0) || dx < 0;
                }

                return false;
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();

        //stop all remaining messages
        Crouton.cancelAllCroutons();
        Tools.setIsVisible(false);
    }

    @Override
    public void onBackPressed() {

        //drawer doesnt exists in android version 4-
        if (!(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB)) {

            final int drawerState = drawer.getDrawerState();

            //if drawer is open, close it
            if (drawerState == MenuDrawer.STATE_OPEN || drawerState == MenuDrawer.STATE_OPENING) {

                drawer.closeMenu();
            } else {

                //ask user if he wants to exit the app
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                builder.setMessage("App beenden?");
                builder.setCancelable(true);
                builder.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        EventBus.getDefault().post(new LogoutEvent());
                        Analytics.tagEvent(getApplicationContext(), "App per Backbutton beendet");

                    }
                });
                builder.setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();


            }
        } else {

            super.onBackPressed();
        }


    }

    public void onEventMainThread(NewUserDataEvent e) {

        handleEvents(e);
    }

    public void onEventMainThread(ShowConversationEvent e) {

        pager.setCurrentItem(3);

    }

    public void onEventMainThread(NotificationEvent e) {

        Tools.getUserData(MainActivity.this);



    }

    public void onEventMainThread(LogoutEvent e) {

        finish();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        this.menu = menu;

        if ((android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB)) {
            MenuItem settings = this.menu.findItem(R.id.settings);
            settings.setVisible(true);
        }

        pnItem = this.menu.findItem(R.id.action_newPn);
        bookmarkItem = this.menu.findItem(R.id.action_newBookmark);

        MenuItemCompat.setActionView(pnItem, R.layout.notification_iconpn);
        MenuItemCompat.setActionView(bookmarkItem, R.layout.notification_iconbook);

        pnActionView = MenuItemCompat.getActionView(pnItem);
        bookActionView = MenuItemCompat.getActionView(bookmarkItem);

        badgeViewPn = (TextView) pnActionView.findViewById(R.id.badge_textview);
        badgeViewBookmark = (TextView) bookActionView.findViewById(R.id.badge_textview);

        //Add clicklistener to Actionbaritems
        pnActionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pager.setCurrentItem(3);
            }
        });

        bookActionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Tools.showBookmarks(MainActivity.this);
            }
        });

        NewUserDataEvent event = EventBus.getDefault().getStickyEvent(NewUserDataEvent.class);
        handleEvents(event);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                drawer.toggleMenu();
                return true;

            case R.id.settings:

                startActivity(new Intent(MainActivity.this, SettingsActivity2.class));

                return true;

        }

        return super.onOptionsItemSelected(item);
    }


    private void handleEvents(NewUserDataEvent e) {

        if (e != null && menu != null) {

            if (e.getNewMessage() > 0) {
                pnItem.setVisible(true);
                if (badgeViewPn != null) {
                    badgeViewPn.setText(e.getNewMessage() + "");
                }

            } else {
                pnItem.setVisible(false);
                pnActionView.setVisibility(View.INVISIBLE);
            }

            if (e.getNewBookmark() > 0) {
                bookmarkItem.setVisible(true);
                if (badgeViewBookmark != null) {
                    badgeViewBookmark.setText(e.getNewBookmark() + "");
                }

            } else {
                bookmarkItem.setVisible(false);
                bookActionView.setVisibility(View.INVISIBLE);
            }

        }

    }

    public void showProgressBar(boolean showProgressBar) {

        if (progressBar != null) {
            if (showProgressBar) {
                progressBar.setVisibility(View.VISIBLE);

            } else {
                progressBar.setVisibility(View.INVISIBLE);

            }

        }

    }




    @Override
    protected void onResume() {
        super.onResume();
        Tools.getUserData(MainActivity.this);
        Tools.setIsVisible(true);

    }


    @Override
    protected void onNewIntent(Intent intent) {

        Tools.getUserData(MainActivity.this);

        Tools.clearNotifications();

        NotificationManager mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.cancel(1);

        if ("OVERVIEW PN".equals(intent.getAction())) {

            pager.setCurrentItem(3);

        } else if ("OVERVIEW BOOKMARKS".equals(intent.getAction())) {

            Tools.showBookmarks(MainActivity.this);

        }

    }

    @Override
    protected void onDestroy() {
        Analytics.upload(getApplicationContext());
        super.onDestroy();

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }
}
