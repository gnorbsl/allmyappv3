package de.allmystery.patric.allmyapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import de.allmystery.patric.allmyapp.fragments.BookmarkFragment;
import de.allmystery.patric.allmyapp.fragments.ConversationFragment;
import de.allmystery.patric.allmyapp.fragments.FriendListFragment;
import de.allmystery.patric.allmyapp.fragments.MainFragment;
import de.allmystery.patric.allmyapp.fragments.MoreFragment;

/**
 * Created by freg on 23.05.2014.
 */
public class MainPagerAdapter extends FragmentPagerAdapter {

    private final String[] TITLES = {"Kategorien", "Beobachtet", "Freundesliste", "Nachrichten",
            "Mehr"};

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return MainFragment.newInstance();
            case 1:
                return new BookmarkFragment();
            case 2:
                return FriendListFragment.newInstance();
            case 3:
                return ConversationFragment.newInstance();
            default:
                return new MoreFragment();

        }

    }

}