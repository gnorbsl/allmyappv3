package de.allmystery.patric.allmyapp.fragments;

import com.crashlytics.android.Crashlytics;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.activities.PnActivity;
import de.allmystery.patric.allmyapp.adapter.ConversationAdapter;
import de.allmystery.patric.allmyapp.events.BookmarkEvent;
import de.allmystery.patric.allmyapp.events.NotificationEvent;
import de.allmystery.patric.allmyapp.events.PnEvent;
import de.allmystery.patric.allmyapp.helper.ActivityAnimator;
import de.allmystery.patric.allmyapp.helper.AllmyApiClient;
import de.allmystery.patric.allmyapp.helper.Conversation;
import de.allmystery.patric.allmyapp.helper.ConversationResponse;
import de.allmystery.patric.allmyapp.helper.Tools;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by freg on 23.07.2014.
 */
public class ConversationFragment extends ListFragment {

    //TODO add activity if user wants more conversations
    private ConversationAdapter adapter;

    private List<Conversation> conversationEntries;

    private PnEvent pnEvent;

    private BookmarkEvent bookmarkEvent;

    private Menu menu;

    private ProgressInterface progressInterface;

    public ConversationFragment() {

    }

    public static ConversationFragment newInstance() {

        ConversationFragment fragment = new ConversationFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            progressInterface = (ProgressInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement Interface");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pnoverview_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);

        if (!EventBus.getDefault().isRegistered(ConversationFragment.this)) {
            EventBus.getDefault().register(ConversationFragment.this);
        }

        conversationEntries = new ArrayList<>();

        adapter = new ConversationAdapter(getActivity(), conversationEntries);
        getListView().setAdapter(adapter);

        getConversations();


    }

    private void getConversations() {



        progressInterface.showProgressBar(true);

        AllmyApiClient.getAllmyApiInterface(getActivity()).getConversations(new Callback<ConversationResponse>() {
            @Override
            public void success(ConversationResponse conversationResponse, Response response) {

                Tools.handleUserdata(conversationResponse.getUserdata());

                progressInterface.showProgressBar(false);

                List<Conversation> conversations = conversationResponse.getConversations();

                conversationEntries.clear();
                conversationEntries.addAll(conversations);

                adapter.notifyDataSetChanged();

            }

            @Override
            public void failure(RetrofitError error) {

                progressInterface.showProgressBar(false);

                Tools.handleError(getActivity(), error);

            }
        });

    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Conversation conversation = conversationEntries.get(position);

        Intent intent = new Intent(getActivity(), PnActivity.class);
        intent.putExtra("de.allmystery.patric.allmyapp.conversation", conversation);
        getActivity().startActivity(intent);
        new ActivityAnimator().fadeAnimation(getActivity());

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.friendlistmenu, menu);
        this.menu = menu;

        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_refreshfriends) {

            Tools.getUserData(getActivity());

            refresh();

            getConversations();

        }
        return true;
    }

    private void refresh() {

        adapter = new ConversationAdapter(getActivity(), conversationEntries);
        getListView().setAdapter(adapter);

        getConversations();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public void onEventMainThread(NotificationEvent e) {

        Log.i("NOTOF", "NODIHD");

        Tools.getUserData(getActivity());

        refresh();
    }

    public interface ProgressInterface {

        public void showProgressBar(boolean showProgressBar);
    }


}
