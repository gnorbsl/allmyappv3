package de.allmystery.patric.allmyapp.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.activities.PnActivity;
import de.allmystery.patric.allmyapp.adapter.FriendListAdapter;
import de.allmystery.patric.allmyapp.helper.ActivityAnimator;
import de.allmystery.patric.allmyapp.helper.AllmyApiClient;
import de.allmystery.patric.allmyapp.helper.Conversation;
import de.allmystery.patric.allmyapp.helper.Friend;
import de.allmystery.patric.allmyapp.helper.FriendListResponse;
import de.allmystery.patric.allmyapp.helper.Tools;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class FriendListFragment extends ListFragment {

    private FriendListAdapter adapter;

    private List<Friend> friendAdapterList;

    private ProgressInterface progressInterface;

    public FriendListFragment() {

    }

    public static FriendListFragment newInstance() {

        FriendListFragment fragment = new FriendListFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            progressInterface = (ProgressInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement Interface");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_friend_list, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //TODO add refresh
        setHasOptionsMenu(true);

        friendAdapterList = new ArrayList<>();
        adapter = new FriendListAdapter(getActivity(), friendAdapterList);
        getListView().setAdapter(adapter);

        getFriends();

    }

    private void getFriends() {

        progressInterface.showProgressBar(true);

        AllmyApiClient.getAllmyApiInterface(getActivity())
                .getFriends(new Callback<FriendListResponse>() {
                    @Override
                    public void success(FriendListResponse friendListResponse, Response response) {

                        Tools.handleUserdata(friendListResponse.getUserdata());

                        progressInterface.showProgressBar(false);

                        ArrayList<Friend> online = new ArrayList<>();
                        ArrayList<Friend> offline = new ArrayList<>();

                        List<Friend> friends = friendListResponse.getFriends();

                        for (Friend friend : friends) {
                            if (friend.isOnline()) {
                                online.add(friend);
                            } else {
                                offline.add(friend);
                            }
                        }

                        friends.clear();
                        friends.addAll(online);
                        friends.addAll(offline);

                        friendAdapterList.clear();
                        friendAdapterList.addAll(friends);

                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                        progressInterface.showProgressBar(false);

                        if (error != null && getActivity() != null) {

                            Tools.handleError(getActivity(), error);

                        }
                    }
                });

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Friend friend = friendAdapterList.get(position);

        Conversation conversation = new Conversation();
        conversation.setUsername(friend.getUsername());
        conversation.setUserid(friend.getUserid());

        Intent intent = new Intent(getActivity(), PnActivity.class);
        intent.putExtra("de.allmystery.patric.allmyapp.conversation", conversation);
        getActivity().startActivity(intent);
        new ActivityAnimator().fadeAnimation(getActivity());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.friendlistmenu, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_refreshfriends) {

            Tools.getUserData(getActivity());

            getFriends();

        }
        return true;
    }

    public interface ProgressInterface {

        public void showProgressBar(boolean showProgressBar);
    }
}
