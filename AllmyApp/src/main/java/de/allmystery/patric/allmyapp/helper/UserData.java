package de.allmystery.patric.allmyapp.helper;

/**
 * Created by freg on 14.12.2014.
 */
public class UserData {

    private int unread_bookmarks;
    private int unread_messages;
    private String username;

    public int getUnread_bookmarks() {
        return unread_bookmarks;
    }

    public int getUnread_messages() {
        return unread_messages;
    }

    public String getUsername() {
        return username;
    }
}
