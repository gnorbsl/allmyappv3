package de.allmystery.patric.allmyapp.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import java.util.HashMap;
import java.util.Map;

import de.allmystery.patric.allmyapp.BuildConfig;
import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.events.LogoutEvent;
import de.allmystery.patric.allmyapp.helper.Analytics;
import de.allmystery.patric.allmyapp.helper.Tools;
import de.greenrobot.event.EventBus;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity2 extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Analytics.tagScreen(getApplicationContext(), "SettingsActivity");

        addPreferencesFromResource(R.xml.pref);

        Preference donate = getPreferenceScreen().findPreference("donate");
        Preference logout = getPreferenceScreen().findPreference("logout");
        Preference version = getPreferenceScreen().findPreference("version");
        Preference showAds = getPreferenceScreen().findPreference("showads");
        Preference feedback = getPreferenceScreen().findPreference("feedback");

        Preference register = getPreferenceScreen().findPreference("register");

        version.setSummary(BuildConfig.VERSION_NAME);

        donate.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                        "https://www.allmystery.de/blogs/fregman/spendenseite_fuer_die_androidapp"));
                SettingsActivity2.this.startActivity(browserIntent);

                Analytics.tagEvent(getApplicationContext(), "Spendenseite aufgerufen");
                return true;
            }
        });

        logout.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                final AlertDialog.Builder dialog = new AlertDialog.Builder(SettingsActivity2.this);
                dialog.setCancelable(true)
                        .setMessage("Ausloggen?")
                        .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                final SharedPreferences pref = getSharedPreferences("userdata",
                                        MODE_PRIVATE);

                                pref.edit().remove("token").apply();

                                EventBus.getDefault().post(new LogoutEvent());

                                Analytics.tagEvent(getApplicationContext(), "logout");

                                finish();

                            }
                        }).setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.cancel();

                    }
                }).create().show();

                return true;
            }
        });

        showAds.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {

                boolean adActivated = Boolean.valueOf(o.toString());

                Map<String, String> values = new HashMap<>();
                values.put("Nutzer", Tools.getUsername(SettingsActivity2.this));

                Analytics.tagEvent(getApplicationContext(),
                        "Werbung " + (adActivated ? "aktiviert" : "deaktiviert"), values);

                return true;
            }
        });

        register.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                Tools.reRegisterGCM(SettingsActivity2.this);

                return true;
            }
        });


    }


    @Override
    protected void onPause() {
        super.onPause();
        Tools.setIsVisible(false);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Tools.setIsVisible(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
