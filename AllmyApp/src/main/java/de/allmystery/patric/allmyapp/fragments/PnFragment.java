package de.allmystery.patric.allmyapp.fragments;


import com.crashlytics.android.Crashlytics;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.adapter.PnListAdapter;
import de.allmystery.patric.allmyapp.helper.AllmyApiClient;
import de.allmystery.patric.allmyapp.helper.Conversation;
import de.allmystery.patric.allmyapp.helper.Message;
import de.allmystery.patric.allmyapp.helper.PnResponse;
import de.allmystery.patric.allmyapp.helper.Tools;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by freg on 28.07.2014.
 */
public class PnFragment extends ListFragment {


    List<Message> pnAdapterList;

    PnListAdapter adapter;

    ListView listView;

    private int pageNumber;

    Conversation conversation;

    public static PnFragment newInstance(int pageNumber) {

        PnFragment f = new PnFragment();

        Bundle args = new Bundle();
        args.putInt("pageNumber", pageNumber);
        f.setArguments(args);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_post, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            pageNumber = args.getInt("pageNumber");

        }

        pnAdapterList = new ArrayList<>();
        adapter = new PnListAdapter(getActivity(), pnAdapterList);
        getListView().setAdapter(adapter);
        listView = getListView();
        listView.setCacheColorHint(Color.TRANSPARENT);

        conversation = getActivity().getIntent().getExtras().getParcelable("de.allmystery.patric.allmyapp.conversation");
        getConversation(conversation.getUserid());

    }


    private void getConversation(int userId) {

        AllmyApiClient.getAllmyApiInterface(getActivity()).getConversation(userId, pageNumber*15, new Callback<PnResponse>() {
            @Override
            public void success(PnResponse pnResponse, Response response) {

                Tools.handleUserdata(pnResponse.getUserdata());

                pnAdapterList.clear();

                pnAdapterList.addAll(pnResponse.getMessages());
                adapter.notifyDataSetChanged();

                if (pageNumber == 0) {
                    listView.setSelection(adapter.getCount() - 1);
                }

            }

            @Override
            public void failure(RetrofitError error) {

                Tools.handleError(getActivity(), error);

            }
        });

    }

}
