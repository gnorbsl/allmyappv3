package de.allmystery.patric.allmyapp.helper;

import java.util.List;

/**
 * Created by freg on 15.12.2014.
 */
public class PostResponse {

    public int getThread_pages() {
        return thread_pages;
    }

    public UserData getUserdata() {
        return userdata;
    }

    public String getThread_category() {
        return thread_category;
    }

    public String getThread_title() {
        return thread_title;
    }

    public boolean isThread_closed() {
        return thread_closed;
    }

    private List<Post> posts;
    private UserData userdata;
    private int thread_pages;
    private String thread_category;
    private String thread_title;
    private boolean thread_closed;

    public List<Post> getPosts() {
        return posts;
    }
}
