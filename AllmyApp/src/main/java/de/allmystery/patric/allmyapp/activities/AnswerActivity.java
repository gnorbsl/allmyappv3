package de.allmystery.patric.allmyapp.activities;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.events.UpdateUIEvent;
import de.allmystery.patric.allmyapp.helper.AllmyApiClient;
import de.allmystery.patric.allmyapp.helper.Analytics;
import de.allmystery.patric.allmyapp.helper.SendPnObject;
import de.allmystery.patric.allmyapp.helper.SendPnResponse;
import de.allmystery.patric.allmyapp.helper.SendPost;
import de.allmystery.patric.allmyapp.helper.SendPostResponse;
import de.allmystery.patric.allmyapp.helper.ThreadObject;
import de.allmystery.patric.allmyapp.helper.Tools;
import de.allmystery.patric.allmyapp.helper.WatchThread;
import de.allmystery.patric.allmyapp.helper.WatchThreadResponse;
import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.LifecycleCallback;
import de.keyboardsurfer.android.widget.crouton.Style;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Patric on 21.06.2014.
 */
public class AnswerActivity extends ActionBarActivity {

    ImageButton smileyButton;

    TextView chooseCategory;

    EditText threadTitle;

    Spinner category;

    EditText answerText;

    Button imageButton;

    Button zitatButton;

    Button kursivButton;

    String text;

    Bundle extras;

    int receiverId;

    boolean pn;

    ThreadObject thread;

    int userId;


    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer);

        smileyButton = (ImageButton) findViewById(R.id.smileyButton);
        answerText = (EditText) findViewById(R.id.answerText);
        imageButton = (Button) findViewById(R.id.bildButton);
        zitatButton = (Button) findViewById(R.id.zitatButton);
        kursivButton = (Button) findViewById(R.id.kursivButton);

        Analytics.tagScreen(getApplicationContext(), "AnswerActivity");

        if ((android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB)) {
            answerText.setTextColor(Color.BLACK);
        }

        this.getSupportActionBar().setTitle("");

        extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey("de.allmystery.patric.allmyapp.thread")) {
                thread = extras.getParcelable("de.allmystery.patric.allmyapp.thread");
                pref = getSharedPreferences("threadAnswer", MODE_PRIVATE);

                if (pref != null && pref.contains(String.valueOf(thread.getThread_id()))) {
                    answerText.setText(pref.getString(String.valueOf(thread.getThread_id()), ""));
                    answerText.setSelection(answerText.length());
                }

                if (extras.containsKey("de.allmystery.patric.allmyapp.newThread")) {

                    chooseCategory = (TextView) findViewById(R.id.chooseCategory);
                    threadTitle = (EditText) findViewById(R.id.threadTitle);
                    category = (Spinner) findViewById(R.id.category);

                    chooseCategory.setVisibility(View.VISIBLE);
                    threadTitle.setVisibility(View.VISIBLE);
                    category.setVisibility(View.VISIBLE);

                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                            R.array.categorieTitles, R.layout.spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    category.setAdapter(adapter);

                }
            }

            if (extras.containsKey("userId")) {
                userId = extras.getInt("userId");

                pref = getSharedPreferences("pnAnswer", MODE_PRIVATE);

                if (pref.contains(String.valueOf(userId))) {
                    answerText.setText(pref.getString(String.valueOf(userId), ""));
                    answerText.setSelection(answerText.length());
                }
            }

            if (extras.containsKey("text")) {
                text = extras.getString("text");
                answerText.append(text + "\n\n");
                answerText.setSelection(answerText.length());

            }
        }

        smileyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Item[] items = {
                        new Item(":X", R.drawable.x),
                        new Item(":|", R.drawable.ratlos),
                        new Item(":)", R.drawable.lachen),
                        new Item(":(", R.drawable.traurig),
                        new Item(";)", R.drawable.spass),
                        new Item(":D", R.drawable.llachen),
                        new Item(":P:", R.drawable.tongue2),
                        new Item(":alien:", R.drawable.alien),
                        new Item(":}", R.drawable.teufel),
                        new Item(":{", R.drawable.kotz),
                        new Item(":cool:", R.drawable.cool),
                        new Item(":fuya:", R.drawable.fuya),
                        new Item(":kiff:", R.drawable.kiffen),
                        new Item(":no:", R.drawable.wrong),
                        new Item(":ok:", R.drawable.ok),
                        new Item(":bier:", R.drawable.beer),
                        new Item(":ask:", R.drawable.ask),
                        new Item(":shot:", R.drawable.shot),
                        new Item(":nessie:", R.drawable.nessie),
                        new Item(":ghost:", R.drawable.ghost),
                        new Item(":N:", R.drawable.thumbs_down),
                        new Item(":Y:", R.drawable.thumbs_up),
                        new Item(":merle:", R.drawable.merle),
                        new Item(":tannenbaum:", R.drawable.tannenbaum),
                        new Item(":dog:", R.drawable.dog),
                        new Item(":kiss:", 0),
                        new Item(":vodka:", R.drawable.vodka),
                        new Item(":nerv:", R.drawable.nerv),
                        new Item(":mexican:", R.drawable.mexican),
                        new Item(":lv:", R.drawable.love),
                        new Item(":troll:", R.drawable.trollface),
                        new Item(":santa:", R.drawable.santa),
                        new Item(":o:", R.drawable.oo),
                        new Item(":hombre:", R.drawable.hombre),
                        new Item(":fuyeah:", R.drawable.fuyeah),
                        new Item(":engel:", R.drawable.engel),
                        new Item(":wein:", R.drawable.wein),
                        new Item(":mozart:", R.drawable.mozart),
                        new Item(":pic:", R.drawable.pic),
                        new Item(":trollbier:", R.drawable.trollbier),
                        new Item(":popcorn:", R.drawable.popcorn),
                        new Item(":trollking:", R.drawable.trollking),
                        new Item(":kingfu:", R.drawable.kingfu),
                        new Item(":tv:", R.drawable.tv),
                        new Item(":kc:", R.drawable.kc),
                        new Item(":aluhut:", 0),
                        new Item(":pony:", 0),
                        new Item(":wicht:", R.drawable.link),
                        new Item(":sleepy:", R.drawable.sleepy),
                        new Item(":kaffee:", R.drawable.kaffee),
                        new Item(":note:", R.drawable.note),
                        new Item(":allmy:", R.drawable.allmy_icon_tiny),
                        new Item(":cry:", R.drawable.cry),
                        new Item(":datrueffel:", R.drawable.datrueffel),
                        new Item(":target:", R.drawable.target),
                        new Item(":mosquito:", R.drawable.mosquito),
                        new Item(":chatter:", R.drawable.chatter),
                        new Item(":rock:", R.drawable.rock),
                        new Item(":vv:", R.drawable.vv),
                        new Item(":cat:", R.drawable.cat),
                        new Item(":mouse:", R.drawable.mouse),
                        new Item(":king:", R.drawable.king),
                        new Item(":chinamann:", R.drawable.chinamann),
                        new Item(":dino:", R.drawable.dino),
                        new Item(":krbis:", R.drawable.krbis),
                        new Item(":trollsanta:", R.drawable.trollsanta),
                        new Item(":ganja:", R.drawable.ganja),
                        new Item(":blackmetal:", R.drawable.blackmetal),
                        new Item(":trashmetal:", R.drawable.trashmetal),
                        new Item(":doommetal:", R.drawable.doommetal),
                        new Item(":deathmetal:", R.drawable.deathmetal),
                        new Item(":folkmetal:", R.drawable.folkmetal),
                        new Item(":powermetal:", R.drawable.powermetal)
                };

                ListAdapter adapter = new ArrayAdapter<Item>(
                        AnswerActivity.this,
                        android.R.layout.select_dialog_item,
                        android.R.id.text1,
                        items) {

                    public View getView(int position, View convertView, ViewGroup parent) {
                        //User super class to create the View
                        View v = super.getView(position, convertView, parent);
                        v.setBackgroundColor(Color.BLACK);
                        TextView tv = (TextView) v.findViewById(android.R.id.text1);
                        tv.setTextColor(Color.WHITE);

                        //Put the image on the TextView
                        tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);

                        //Add margin between image and text (support various screen densities)
                        int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                        tv.setCompoundDrawablePadding(dp5);

                        return v;
                    }
                };

                new AlertDialog.Builder(AnswerActivity.this)
                        .setTitle("Smiley wählen")

                        .setAdapter(adapter, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {

                                Log.i("SMILEY", items[item].toString());
                                insertBB(items[item].toString(), "");


                            }
                        }).show();

            }
        });

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                insertBB("[IMG]", "[/IMG]");
            }
        });

        zitatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insertBB("[ZITAT]", "[/ZITAT]");
            }
        });

        kursivButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insertBB("[I]", "[/I]");
            }
        });

    }


    protected void insertBB(String tagBegin, String tagEnd) {

        int startSelection = answerText.getSelectionStart();
        int endSelection = answerText.getSelectionEnd();

        String selectedText = answerText.getText().toString()
                .substring(startSelection, endSelection);

        if (selectedText.isEmpty()) {
            selectedText = " ";
        }
        String tagText = tagBegin + selectedText + tagEnd;

        if (selectedText.isEmpty()) {

            int start = Math.max(answerText.getSelectionStart(), 0);
            int end = Math.max(answerText.getSelectionEnd(), 0);
            answerText.getText().replace(Math.min(start, end), Math.max(start, end),
                    tagBegin + tagEnd, 0, (tagBegin + tagEnd).length());

        } else {

            int start = Math.max(answerText.getSelectionStart(), 0);
            int end = Math.max(answerText.getSelectionEnd(), 0);
            answerText.getText().replace(Math.min(start, end), Math.max(start, end),
                    tagText, 0, tagText.length());


        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.answermenu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getTitle().equals("senden")) {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AnswerActivity.this);

            if (threadTitle != null && threadTitle.getText().toString().equals("")) {
                alertDialogBuilder.setTitle("Threadtitel wählen!");

                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                alertDialogBuilder.create().show();
            } else {

                if (userId > 0) {
                    alertDialogBuilder.setTitle("Pn absenden?");
                    receiverId = userId;
                    pn = true;
                } else if (thread != null) {
                    alertDialogBuilder.setTitle("Beitrag absenden?");
                    receiverId = thread.getThread_id();
                    pn = false;
                }

                alertDialogBuilder
                        .setCancelable(true)
                        .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                String messageOrText = "";
                                if (pn) {

                                    sendPn();
                                } else {

                                    sendAnswer();
                                }

                            }
                        })
                        .setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Analytics
                                        .tagEvent(getApplicationContext(), "Antworten abgebrochen");

                                dialog.cancel();

                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();

                alertDialog.show();


            }
        }

        return true;

    }

    private void sendPn() {

        AllmyApiClient.getAllmyApiInterface(getApplicationContext())
                .sendPn(new SendPnObject(answerText.getText().toString()),
                        userId, new Callback<SendPnResponse>() {
                            @Override
                            public void success(SendPnResponse sendPnResponse,
                                    Response response) {

                                if (!sendPnResponse.getMessage().equals("")) {
                                    Analytics.tagEvent(getApplicationContext(), "Pn gesendet");
                                    handleSuccess(userId, "Nachricht");
                                } else {
                                    showPnError(sendPnResponse);
                                }

                            }

                            @Override
                            public void failure(RetrofitError error) {

                                Tools.handleError(AnswerActivity.this, error);

                            }
                        });


    }

    private void showPnError(SendPnResponse sendPnResponse) {

        Crouton.makeText(AnswerActivity.this,
                (getString(R.string.pn_send_error) + sendPnResponse.getError()),
                Style.ALERT).show();

    }

    private void sendAnswer() {

        String title = null;
        String cat = null;
        if (threadTitle != null) {
            title = threadTitle.getText().toString();

            String[] cats = getResources().getStringArray(R.array.categories);

            cat = cats[category.getSelectedItemPosition()];
        }

        AllmyApiClient.getAllmyApiInterface(getApplicationContext())
                .sendPost(new SendPost(answerText.getText().toString(), title, cat),
                        thread.getThread_id(),
                        new Callback<SendPostResponse>() {
                            @Override
                            public void success(
                                    SendPostResponse sendPostResponse,
                                    Response response) {

                                Log.i("ANSWER", response.getBody().toString());
                                if (sendPostResponse.isSuccess()) {

                                    Analytics.tagEvent(getApplicationContext(), "Beitrag gesendet");
                                    handleSuccess(
                                            thread.getThread_id(),
                                            "Beitrag");

                                    SharedPreferences sharedPref = PreferenceManager
                                            .getDefaultSharedPreferences(AnswerActivity.this);

                                    if (sharedPref.getBoolean("watchThreads", false)
                                            && thread.getThread_id() != 1) {

                                        AllmyApiClient.getAllmyApiInterface(AnswerActivity.this)
                                                .watchThread(new WatchThread(thread.getThread_id()),
                                                        new Callback<WatchThreadResponse>() {
                                                            @Override
                                                            public void success(
                                                                    WatchThreadResponse watchThreadResponse,
                                                                    Response response) {
                                                                //TODO

                                                            }

                                                            @Override
                                                            public void failure(
                                                                    RetrofitError error) {

                                                                //TODO
                                                            }
                                                        });

                                    }


                                } else {
                                    showPostError(sendPostResponse);
                                }

                            }

                            @Override
                            public void failure(RetrofitError error) {

                                Tools.handleError(AnswerActivity.this, error);

                            }
                        });


    }

    private void showPostError(SendPostResponse sendPostResponse) {

        Crouton.makeText(AnswerActivity.this,
                (getString(R.string.post_send_error) + sendPostResponse.getError()),
                Style.ALERT).show();

    }

    private void handleSuccess(int id, String art) {

        pref.edit().remove(String.valueOf(id)).apply();

        Crouton crouton = Crouton
                .makeText(AnswerActivity.this, art + " erfolgreich gesendet", Style.CONFIRM);

        Configuration.Builder config = new Configuration.Builder();
        config.setDuration(1000);
        crouton.setConfiguration(config.build());
        crouton.setLifecycleCallback(new LifecycleCallback() {


            @Override
            public void onDisplayed() {

            }

            @Override
            public void onRemoved() {

                AnswerActivity.this.finish();

            }
        });

        crouton.show();
        EventBus.getDefault().post(new UpdateUIEvent());
    }

    @Override
    public void onBackPressed() {

        pref.edit().putString(
                String.valueOf(thread != null ? thread.getThread_id() : userId),
                answerText.getText().toString()).apply();
        super.onBackPressed();
    }

    @Override
    protected void onPause() {

        super.onPause();
        Tools.setIsVisible(false);

    }

    public static class Item {

        public final String text;

        public final int icon;

        public Item(String text, Integer icon) {
            this.text = text;
            this.icon = icon;
        }

        @Override
        public String toString() {
            return text;
        }
    }


}
