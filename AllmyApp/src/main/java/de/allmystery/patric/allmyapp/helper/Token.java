package de.allmystery.patric.allmyapp.helper;

/**
 * Created by freg on 16.12.2014.
 */
public class Token {

    private String token;

    public String getToken() {
        return token;
    }

    public Token(String token) {
        this.token = token;
    }
}
