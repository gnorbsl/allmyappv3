package de.allmystery.patric.allmyapp.helper;

/**
 * Created by freg on 14.12.2014.
 */
public class LoginResponse {

    private boolean success;

    private String token;

    private UserData userData;

    private String error;

    public boolean isSuccess() {
        return success;
    }

    public String getToken() {
        return token;
    }

    public UserData getUserData() {
        return userData;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
