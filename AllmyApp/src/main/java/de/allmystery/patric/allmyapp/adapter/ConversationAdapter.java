package de.allmystery.patric.allmyapp.adapter;

import com.readystatesoftware.viewbadger.BadgeView;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.helper.Conversation;

/**
 * Created by Patric on 29.05.2014.
 */
public class ConversationAdapter extends ArrayAdapter<Conversation> {

    Context context;

    List<Conversation> conversationEntries;


    public ConversationAdapter(Context context, List<Conversation> conversationEntries) {

        super(context, 0, conversationEntries);

        this.context = context;
        this.conversationEntries = conversationEntries;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Conversation conversation = conversationEntries.get(position);

        View row = convertView;
        ViewHolder holder = new ViewHolder();

        if (row == null) {

            LayoutInflater inflater = LayoutInflater.from(context);
            row = inflater.inflate(R.layout.pnoverview_list_item, parent, false);

            holder.avatar = (ImageView) row.findViewById(R.id.avatar);
            holder.username = (TextView) row.findViewById(R.id.username);
            holder.time = (TextView) row.findViewById(R.id.timeinfo);
            holder.badge = (View) row.findViewById(R.id.invisible_for_badge_view);

            row.setTag(holder);


        } else {
            holder = (ViewHolder) row.getTag();
        }

        Picasso.with(context).load(conversation.getPic()).placeholder(R.drawable.allmystery_bg)
                .into(holder.avatar);

        holder.username.setText(conversation.getUsername());

        holder.time.setText(conversation.getDate());

        BadgeView badge = new BadgeView(context, holder.badge);
        if (conversation.getUnread_count() > 0) {

            badge.setText(String.valueOf(conversation.getUnread_count()));
            badge.setBadgePosition(BadgeView.POSITION_TOP_LEFT);
            badge.setBadgeMargin(1, 1);
            badge.show(true);

        }

        //even odd background colors
        if (position % 2 == 0) {
            row.setBackgroundResource(R.drawable.listview_selector_even);
        } else {
            row.setBackgroundResource(R.drawable.listview_selector_odd);
        }

        return row;
    }

    @Override
    public int getViewTypeCount() {

        return conversationEntries.size() > 0 ? conversationEntries.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    static class ViewHolder {

        ImageView avatar;

        View badge;

        TextView username;

        TextView time;

    }
}
