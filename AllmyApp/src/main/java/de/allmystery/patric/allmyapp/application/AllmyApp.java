package de.allmystery.patric.allmyapp.application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import android.app.Application;

import java.util.HashMap;

import de.allmystery.patric.allmyapp.R;


/**
 * Created by freg on 19.11.2014.
 */
public class AllmyApp extends Application {


    private static final String PROPERTY_ID = "UA-56883835-2";

    private static final String TAG = "AllmyApp";

    public static int GENERAL_TRACKER = 0;

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    public AllmyApp() {
        super();
    }

    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics
                    .newTracker(R.xml.app_tracker)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics
                            .newTracker(PROPERTY_ID) : analytics
                            .newTracker(R.xml.global_tracker);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }

    public enum TrackerName {
        APP_TRACKER, GLOBAL_TRACKER, ECOMMERCE_TRACKER,
    }
}


