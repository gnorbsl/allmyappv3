package de.allmystery.patric.allmyapp.activities;

import com.google.android.gms.analytics.Tracker;

import com.crashlytics.android.Crashlytics;
import com.iangclifton.android.floatlabel.FloatLabel;
import com.localytics.android.LocalyticsAmpSession;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.helper.ActivityAnimator;
import de.allmystery.patric.allmyapp.helper.AllmyApiClient;
import de.allmystery.patric.allmyapp.helper.Analytics;
import de.allmystery.patric.allmyapp.helper.Constants;
import de.allmystery.patric.allmyapp.helper.LimitedOnClickListener;
import de.allmystery.patric.allmyapp.helper.LoginResponse;
import de.allmystery.patric.allmyapp.helper.Tools;
import de.allmystery.patric.allmyapp.helper.User;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginActivity extends FragmentActivity {

    Tracker tracker;

    SharedPreferences pref;

    private SmoothProgressBar progressBar;

    private FloatLabel usernameField;

    private FloatLabel passwordField;

    private CheckBox saveUsername;

    private CheckBox savePassword;

    private Button loginButton;

    private LocalyticsAmpSession mLocalyticsAmpSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_skiplogin);

        Crashlytics.start(LoginActivity.this);

        pref = getSharedPreferences("userdata", MODE_PRIVATE);

        if (!pref.contains("username")) {
            pref.edit().remove("token").apply();
        }

        Analytics.register(LoginActivity.this);

        //check if device is a tablet or not
        checkIfTablet();

        startLogin();

    }

    private void checkIfTablet() {

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {
            Tools.setHasLargeScreen(true);
        } else {
            Tools.setHasLargeScreen(false);
        }
    }


    private void startLogin() {

        //skip login if token exists
        if (pref.contains("token")) {

            Analytics.setName(LoginActivity.this, pref.getString("username", "N/A"));
            Crashlytics.setUserName(pref.getString("username", "N/A"));

            Constants.setToken(pref.getString("token", ""));
            AllmyApiClient.setToken(pref.getString("token", ""));

            startMainActivity();

        } else {

            setContentView(R.layout.activity_login);
            initViews();

            loginButton.setOnClickListener(new LimitedOnClickListener() {

                String username;

                String password;

                @Override
                public void onSingleClick(View view) {

                    if (validateFields()) {

                        login(username, password);

                    }

                }

                //validating the inputfields
                private boolean validateFields() {

                    username = usernameField.getEditText().getText().toString();
                    password = passwordField.getEditText().getText().toString();

                    if (username.isEmpty()) {
                        usernameField.getEditText().setError("Feld darf nicht leer sein");
                        usernameField.requestFocus();
                    }
                    if (password.isEmpty()) {
                        passwordField.getEditText().setError("Feld darf nicht leer sein");
                        usernameField.requestFocus();
                    }

                    return !username.isEmpty() && !password.isEmpty();
                }
            });

        }

    }

    private void initViews() {

        progressBar = (SmoothProgressBar) findViewById(R.id.progress);
        loginButton = (Button) findViewById(R.id.loginButton);
        usernameField = (FloatLabel) findViewById(R.id.username);
        passwordField = (FloatLabel) findViewById(R.id.password);

        saveUsername = (CheckBox) findViewById(R.id.saveUsername);
        savePassword = (CheckBox) findViewById(R.id.savePassword);

        if (pref.getBoolean("saveUsername", false)) {
            saveUsername.setChecked(true);

            usernameField.getEditText().setText(pref.getString("username", ""));
        } else {
            saveUsername.setChecked(false);

        }

        if (pref.getBoolean("savePassword", false)) {
            savePassword.setChecked(true);

            passwordField.getEditText().setText(pref.getString("password", ""));
        } else {
            savePassword.setChecked(false);
        }

        if ((android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB)) {
            usernameField.getEditText().setTextColor(Color.BLACK);
            passwordField.getEditText().setTextColor(Color.BLACK);
        }

    }


    private void login(final String username, final String password) {

        progressBar.setVisibility(View.VISIBLE);

        AllmyApiClient.getAllmyApiInterface(getApplicationContext())
                .login(new User(username, password), new Callback<LoginResponse>() {
                    @Override
                    public void success(LoginResponse loginResponse, Response response) {

                        progressBar.setVisibility(View.INVISIBLE);

                        if (loginResponse.isSuccess()) {

                            //add token as header
                            AllmyApiClient.setToken(loginResponse.getToken());

                            storeTokenInPreferences(loginResponse.getToken());

                            //identifier for errorreporting
                            Crashlytics.setUserName(username);

                            Analytics.setName(LoginActivity.this, username);
                            Analytics.tagEvent(LoginActivity.this, "Login");

                            pref.edit().putString("username", username).apply();
                            if (saveUsername.isChecked()) {

                                pref.edit().putBoolean("saveUsername", true)
                                        .apply();

                            } else {
                                pref.edit().remove("saveUsername").apply();
                            }

                            if (savePassword.isChecked()) {
                                pref.edit().putString("password", password)
                                        .putBoolean("savePassword", true)
                                        .apply();
                            } else {
                                pref.edit().remove("password").remove("savePassword").apply();
                            }

                            startMainActivity();

                        } else {

                            if ("invalid login credentials".equals(loginResponse.getError())) {
                                Crouton.makeText(LoginActivity.this, getString(R.string.wronglogin),
                                        Style.ALERT).show();
                            }
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {

                        progressBar.setVisibility(View.INVISIBLE);

                        Tools.handleError(LoginActivity.this, error);
                    }
                });
    }

    private void storeTokenInPreferences(String token) {

        Constants.setToken(token);

        SharedPreferences.Editor edit = pref.edit();
        edit.putString("token", token);
        edit.apply();

    }

    @Override
    protected void onPause() {
        super.onPause();

        //stop all remaining messages
        Crouton.cancelAllCroutons();

    }

    private void startMainActivity() {

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        new ActivityAnimator().fadeAnimation(LoginActivity.this);

        finish();
    }

}
