package de.allmystery.patric.allmyapp.helper;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by freg on 14.12.2014.
 */
public class Friend implements Parcelable {

    private String username;
    private boolean busy;
    private String pic;
    private String status_msg;
    private boolean online;
    private int userid;
    private boolean mobile;


    public String getUsername() {
        return username;
    }

    public boolean isBusy() {
        return busy;
    }

    public String getPic() {
        return pic;
    }

    public String getStatus_msg() {
        return status_msg;
    }

    public boolean isOnline() {
        return online;
    }

    public int getUserid() {
        return userid;
    }

    public boolean isMobile() {
        return mobile;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.username);
        dest.writeByte(busy ? (byte) 1 : (byte) 0);
        dest.writeString(this.pic);
        dest.writeString(this.status_msg);
        dest.writeByte(online ? (byte) 1 : (byte) 0);
        dest.writeInt(this.userid);
        dest.writeByte(mobile ? (byte) 1 : (byte) 0);
    }

    public Friend() {
    }

    private Friend(Parcel in) {
        this.username = in.readString();
        this.busy = in.readByte() != 0;
        this.pic = in.readString();
        this.status_msg = in.readString();
        this.online = in.readByte() != 0;
        this.userid = in.readInt();
        this.mobile = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Friend> CREATOR = new Parcelable.Creator<Friend>() {
        public Friend createFromParcel(Parcel source) {
            return new Friend(source);
        }

        public Friend[] newArray(int size) {
            return new Friend[size];
        }
    };
}
