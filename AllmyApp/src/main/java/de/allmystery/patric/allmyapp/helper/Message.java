package de.allmystery.patric.allmyapp.helper;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by freg on 16.12.2014.
 */
public class Message implements Parcelable {

    public int getMessage_id() {
        return message_id;
    }

    public int getUserid() {
        return userid;
    }

    public String getDate() {
        return Tools.convertTimeString(date);
    }

    public String getMessage() {
        return message;
    }

    public String getPic() {
        return pic;
    }

    public String getUsername() {
        return username;
    }

    private int message_id;
    private int userid;
    private String date;

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;
    private String pic;
    private String username;

    public Message() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.message_id);
        dest.writeInt(this.userid);
        dest.writeString(this.date);
        dest.writeString(this.message);
        dest.writeString(this.pic);
        dest.writeString(this.username);
    }

    private Message(Parcel in) {
        this.message_id = in.readInt();
        this.userid = in.readInt();
        this.date = in.readString();
        this.message = in.readString();
        this.pic = in.readString();
        this.username = in.readString();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        public Message createFromParcel(Parcel source) {
            return new Message(source);
        }

        public Message[] newArray(int size) {
            return new Message[size];
        }
    };
}
