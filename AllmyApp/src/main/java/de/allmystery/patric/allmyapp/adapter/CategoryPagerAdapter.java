package de.allmystery.patric.allmyapp.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import de.allmystery.patric.allmyapp.fragments.CategoryFragment;
import de.allmystery.patric.allmyapp.fragments.PnFragment;
import de.allmystery.patric.allmyapp.fragments.ThreadFragment;
import de.allmystery.patric.allmyapp.helper.Category;
import de.allmystery.patric.allmyapp.helper.ThreadObject;
import de.allmystery.patric.allmyapp.helper.Tools;

/**
 * Created by Patric on 29.05.2014.
 */
public class CategoryPagerAdapter extends FragmentStatePagerAdapter {

    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    private Category category;

    private ThreadObject thread;

    private int pages;

    private Bundle extra;

    public CategoryPagerAdapter(FragmentManager fm, int pages, Bundle extra) {

        super(fm);

        this.pages = pages;
        this.extra = extra;

    }

    public CategoryPagerAdapter(FragmentManager supportFragmentManager, int pages,
            Category category) {
        super(supportFragmentManager);

        this.pages = pages;
        this.category = category;
    }

    public CategoryPagerAdapter(FragmentManager supportFragmentManager, int pages,
            ThreadObject thread) {

        super(supportFragmentManager);

        this.pages = pages;
        this.thread = thread;
    }

    @Override
    public Fragment getItem(int position) {

        if (category != null || (extra != null && extra.containsKey("cat"))) {

            return CategoryFragment.newInstance(position);
        } else if (thread != null || extra.containsKey("threadId")) {

            return ThreadFragment.newInstance(position, this);
        } else {

            if (extra.containsKey("de.allmystery.patric.allmyapp.conversation")) {
                return PnFragment.newInstance(position);
            } else {
                return CategoryFragment.newInstance(position);
            }
        }


    }

    @Override
    public int getCount() {
        return pages;
    }


    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public float getPageWidth(int position) {

        if (Tools.hasLargeScreen() && ((extra != null && !extra.containsKey("threadId"))
                || thread != null) && Tools.getOrientation() == 2) {
            return (0.5f);
        } else {
            return super.getPageWidth(position);
        }

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }


}
