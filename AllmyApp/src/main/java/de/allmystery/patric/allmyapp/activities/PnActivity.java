package de.allmystery.patric.allmyapp.activities;


import com.ToxicBakery.viewpager.transforms.ZoomOutSlideTransformer;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.adapter.CategoryPagerAdapter;
import de.allmystery.patric.allmyapp.events.LogoutEvent;
import de.allmystery.patric.allmyapp.events.UpdateUIEvent;
import de.allmystery.patric.allmyapp.fragments.PageHandlerFragment;
import de.allmystery.patric.allmyapp.helper.ActivityAnimator;
import de.allmystery.patric.allmyapp.helper.AllmyApiClient;
import de.allmystery.patric.allmyapp.helper.Analytics;
import de.allmystery.patric.allmyapp.helper.Conversation;
import de.allmystery.patric.allmyapp.helper.LimitedOnClickListener;
import de.allmystery.patric.allmyapp.helper.PnResponse;
import de.allmystery.patric.allmyapp.helper.SendPnObject;
import de.allmystery.patric.allmyapp.helper.SendPnResponse;
import de.allmystery.patric.allmyapp.helper.Tools;
import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by freg on 24.07.2014.
 */
public class PnActivity extends ActionBarActivity implements PageHandlerFragment.ClickListener {

    ImageButton answerButton;

    ViewPager pager;

    EditText pnText;

    int pages;

    PageHandlerFragment frag;

    SmoothProgressBar progress;

    Bundle extras;

    Conversation conversation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Analytics.tagScreen(getApplicationContext(), "PnActivity");

        setContentView(R.layout.activity_pn);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        if (getIntent().getAction() != null) {
            Tools.clearNotifications();
            NotificationManager mNotificationManager = (NotificationManager)
                    this.getSystemService(Context.NOTIFICATION_SERVICE);

            mNotificationManager.cancel(1);
        }

        Tools.setActivity(PnActivity.this);

        extras = getIntent().getExtras();

        if (extras != null) {

            conversation = extras.getParcelable("de.allmystery.patric.allmyapp.conversation");

            getSupportActionBar().setTitle(conversation.getUsername());
        }

        progress = (SmoothProgressBar) findViewById(R.id.progress);

        EventBus.getDefault().register(this);

        LinearLayout pnAnswer = (LinearLayout) findViewById(R.id.answerPnField);

        answerButton = (ImageButton) findViewById(R.id.pnAnswerButton);
        pnText = (EditText) findViewById(R.id.pnText);

        if (conversation.getUsername().equalsIgnoreCase("system")) {

            pnAnswer.setVisibility(View.GONE);

        }

        pager = (ViewPager) findViewById(R.id.viewPagerCategory);

        frag = (PageHandlerFragment)
                getSupportFragmentManager().findFragmentById(R.id.pageHandlerFragment);

        getPages();

        answerButton.setOnClickListener(new LimitedOnClickListener() {
            @Override
            public void onSingleClick(View v) {

                progress.setVisibility(View.VISIBLE);
                sendPn();
            }
        });

        if ((android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB)) {
            pnText.setTextColor(Color.BLACK);

        }
    }

    private void sendPn() {

        progress.setVisibility(View.VISIBLE);

        AllmyApiClient.getAllmyApiInterface(getApplicationContext())
                .sendPn(new SendPnObject(pnText.getText().toString()), conversation.getUserid(),
                        new Callback<SendPnResponse>() {
                            @Override
                            public void success(SendPnResponse sendPnResponse, Response response) {

                                progress.setVisibility(View.INVISIBLE);

                                if (!sendPnResponse.getMessage().equals("")) {

                                    Analytics.tagEvent(getApplicationContext(), "Pn gesendet");
                                    Crouton.makeText(PnActivity.this, getString(R.string.pn_send),
                                            Style.CONFIRM).show();

                                    pnText.setText("");
                                    refresh();

                                } else {

                                    Crouton.makeText(PnActivity.this,
                                            getString(R.string.pn_send_error) + sendPnResponse
                                                    .getError()
                                            , Style.ALERT);

                                }

                            }

                            @Override
                            public void failure(RetrofitError error) {

                                progress.setVisibility(View.INVISIBLE);

                                Tools.handleError(PnActivity.this, error);

                            }
                        });

    }

    private void getPages() {

        AllmyApiClient.getAllmyApiInterface(getApplicationContext())
                .getConversation(conversation.getUserid(), new Callback<PnResponse>() {
                    @Override
                    public void success(PnResponse pnResponse, Response response) {

                        pages = (int) Math.ceil(pnResponse.getCount() / 15.0);

                        pager.setAdapter(new CategoryPagerAdapter(
                                PnActivity.this.getSupportFragmentManager(), pages,
                                getIntent().getExtras()));
                        handlePageNavigation(pages);

                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });

    }

    private void handlePageNavigation(int pages) {

        if (pages == 0) {
            pages = 1;
        }

        frag.handleNavigation(pager.getCurrentItem(), pages);

        if (Tools.getOrientation() == 1) {

            pager.setPageTransformer(true, new ZoomOutSlideTransformer());
        }

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                    int positionOffsetPixels) {
                frag.changePosition((position + 1), positionOffset);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.pnmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_sendpn:

                Intent intent = new Intent(PnActivity.this, AnswerActivity.class);
                intent.putExtra("userId", conversation.getUserid());

                startActivity(intent);
                new ActivityAnimator().fadeAnimation(PnActivity.this);
                break;

            case R.id.action_refreshpn:

                refresh();
                break;

            case android.R.id.home:

                finish();

                break;

        }

        return super.onOptionsItemSelected(item);
    }

    public void refresh() {

        if (pager != null && pager.getAdapter() != null) {
            pager.getAdapter().notifyDataSetChanged();
        }
    }


    public void onEventMainThread(UpdateUIEvent e) {

        refresh();
    }

    public void onEventMainThread(LogoutEvent e) {

        finish();

    }


    @Override
    public void onNavClick(int position) {
        pager.setCurrentItem((position - 1));
    }

    @Override
    protected void onPause() {
        super.onPause();
        Tools.setIsVisible(false);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Tools.setIsVisible(true);

    }
}
