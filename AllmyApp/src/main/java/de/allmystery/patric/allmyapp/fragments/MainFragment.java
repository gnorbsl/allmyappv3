package de.allmystery.patric.allmyapp.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.activities.CategoryActivity;
import de.allmystery.patric.allmyapp.adapter.MainListAdapter;
import de.allmystery.patric.allmyapp.helper.ActivityAnimator;
import de.allmystery.patric.allmyapp.helper.AllmyApiClient;
import de.allmystery.patric.allmyapp.helper.Category;
import de.allmystery.patric.allmyapp.helper.LimitedOnClickListener;
import de.allmystery.patric.allmyapp.helper.MainResponse;
import de.allmystery.patric.allmyapp.helper.Tools;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Patric on 24.05.2014.
 */
public class MainFragment extends ListFragment {

    private static int currentView = 0;

    private Button all, last, own;

    private Button[] buttons;

    private MainListAdapter adapter;

    private List<Category> categoriesAdapterList;

    private ProgressInterface progressInterface;

    public static MainFragment newInstance() {

        MainFragment f = new MainFragment();
        return f;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            progressInterface = (ProgressInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement Interface");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_category, container, false);

        all = (Button) view.findViewById(R.id.buttonallcategories);
        last = (Button) view.findViewById(R.id.buttonlastposts);
        own = (Button) view.findViewById(R.id.buttonowncategories);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        categoriesAdapterList = new ArrayList<>();

        adapter = new MainListAdapter(getActivity(), categoriesAdapterList);

        getListView().setAdapter(adapter);
        getListView().setCacheColorHint(Color.TRANSPARENT);

        //store buttons to loop through them
        buttons = new Button[]{all, last, own};

        //handle first initialization of fragment and show previous setting
        switch (currentView) {
            case 0:
                toggleButton(all);
                getCategories(false, false);
                break;
            case 1:
                toggleButton(last);
                getCategories(true, false);
                break;
            case 2:
                toggleButton(own);
                getCategories(false, true);
                break;

        }

        all.setOnClickListener(new LimitedOnClickListener() {

            @Override
            public void onSingleClick(View v) {

                currentView = 0;
                toggleButton(all);
                getCategories(false, false);


            }


        });

        last.setOnClickListener(new LimitedOnClickListener() {
            @Override
            public void onSingleClick(View view) {

                currentView = 1;
                toggleButton(last);
                getCategories(true, false);

            }
        });

        own.setOnClickListener(new LimitedOnClickListener() {
            @Override
            public void onSingleClick(View view) {

                currentView = 2;
                toggleButton(own);
                getCategories(false, true);

            }
        });


    }


    private void getCategories(boolean isSorted, boolean showOwn) {

        progressInterface.showProgressBar(true);

        String sort = "asc";
        String own = "";

        if (isSorted) {
            sort = "desc";
        }

        if (showOwn) {
            own = "_my";
        }

        AllmyApiClient.getAllmyApiInterface(getActivity())
                .listCategories(own, sort, new Callback<MainResponse>() {
                    @Override
                    public void success(MainResponse mainResponse, Response response) {

                        progressInterface.showProgressBar(false);

                        categoriesAdapterList.clear();
                        categoriesAdapterList.addAll(mainResponse.getCategories());

                        adapter.notifyDataSetChanged();

                        Tools.handleUserdata(mainResponse.getUserdata());


                    }

                    @Override
                    public void failure(RetrofitError error) {

                        progressInterface.showProgressBar(false);

                        if (error != null && getActivity() != null) {

                            Tools.handleError(getActivity(), error);

                        }
                    }
                });

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        //super.onListItemClick(l, v, position, id);

        Category category = categoriesAdapterList.get(position);

        Intent i = new Intent(getActivity(), CategoryActivity.class);
        i.putExtra("de.allmystery.patric.allmyapp.category", category);
        startActivity(i);

        new ActivityAnimator().fadeAnimation(getActivity());
    }


    private void toggleButton(Button activeButton) {

        for (Button button : buttons) {
            button.setTextColor(activeButton.equals(button) ? Color.WHITE : Color.BLACK);
        }


    }

    public interface ProgressInterface {

        public void showProgressBar(boolean showProgressBar);
    }


}
