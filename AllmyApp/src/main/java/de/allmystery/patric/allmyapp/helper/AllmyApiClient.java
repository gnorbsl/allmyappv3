package de.allmystery.patric.allmyapp.helper;

import android.content.Context;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by freg on 14.12.2014.
 */
public class AllmyApiClient {

    private static AllmyApiInterface allmyApiService;

    private static String token;

    private static RestAdapter restAdapter;

    public static AllmyApiInterface getAllmyApiInterface(Context context) {
        if (allmyApiService == null) {

            createRestAdapter(context);

        }

        return allmyApiService;

    }

    private static void createRestAdapter(final Context context) {

        restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.allmystery.de")
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {

                        if (token == null) {
                            token = Constants.getTOKEN(context);
                        }

                        if (token != null) {

                            request.addHeader("Cookie", "token=" + token);

                        }
                        request.addHeader("useragent", Constants.USERAGENT);
                    }
                })
                .build();

        allmyApiService = restAdapter.create(AllmyApiInterface.class);
    }

    public static void setToken(String token) {

        AllmyApiClient.token = token;
    }

    public interface AllmyApiInterface {

        @POST("/login/")
        void login(@Body User user, Callback<LoginResponse> loginResponse);

        @GET("/category/list{own}")
        void listCategories(@Path("own") String own, @Query("sort") String sort,
                Callback<MainResponse> callback);

        @GET("/friends")
        void getFriends(Callback<FriendListResponse> callback);

        @GET("/conversations")
        void getConversations(Callback<ConversationResponse> callback);

        @GET("/category/{category}")
        void getCategory(@Path("category") String category, @Query("offset") int offset,
                @Query("query") String query, Callback<CategoryResponse> callback);

        @GET("/category/{category}")
        void getCategory(@Path("category") String category, @Query("query") String query,
                Callback<CategoryResponse> callback);

        @GET("/category/{category}")
        void getCategory(@Path("category") String category, Callback<CategoryResponse> callback);

        @GET("/userdata")
        void getUserData(Callback<UserDataResponse> callback);

        @GET("/thread/{threadId}/")
        void getThread(@Path("threadId") int threadId, @Query("page") int pageNumber,
                Callback<PostResponse> callback);

        @POST("/bookmarks/")
        void watchThread(@Body WatchThread watchThread, Callback<WatchThreadResponse> callback);

        @POST("/bookmarks/")
        void unWatchThread(@Body UnWatchThread watchThread, Callback<WatchThreadResponse> callback);

        @POST("/conversation/{userId}")
        void sendPn(@Body SendPnObject pnObject, @Path("userId") int userId,
                Callback<SendPnResponse> callback);

        @POST("/thread/{threadId}")
        void sendPost(@Body SendPost sendPost, @Path("threadId") int threadId,
                Callback<SendPostResponse> callback);

        @GET("/conversation/{userId}/")
        void getConversation(@Path("userId") int userId, @Query("offset") int pageNumber,
                Callback<PnResponse> callback);

        @GET("/conversation/{userId}/")
        void getConversation(@Path("userId") int userId, Callback<PnResponse> callback);


        @POST("/update_gcm")
        void registerGcm(@Body Token token, Callback<TokenResponse> callback);
    }

}

