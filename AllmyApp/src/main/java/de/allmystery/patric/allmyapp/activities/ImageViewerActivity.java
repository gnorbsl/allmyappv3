package de.allmystery.patric.allmyapp.activities;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.helper.Analytics;
import de.allmystery.patric.allmyapp.helper.Tools;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import uk.co.senab.photoview.PhotoView;

/**
 * Created by Patric on 10.06.2014.
 */
public class ImageViewerActivity extends ActionBarActivity {

    Bundle extras;

    String url;

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
            new Thread(new Runnable() {
                @Override
                public void run() {

                    String[] temp = url.split("/");
                    String fileName = temp[temp.length - 1].replaceAll("[^a-zA-Z0-9\\.\\-]", "_");
                    File dir = new File(
                            Environment.getExternalStorageDirectory().getPath() + "/AllmyApp/");
                    dir.mkdirs();
                    File file = new File(
                            Environment.getExternalStorageDirectory().getPath() + "/AllmyApp/"
                                    + fileName);
                    try {
                        file.createNewFile();
                        FileOutputStream ostream = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, ostream);
                        ostream.close();
                        Crouton.makeText(ImageViewerActivity.this,
                                "Bild gespeichert unter " + file.getPath(), Style.INFO).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Crouton.makeText(ImageViewerActivity.this, "Fehler beim speichern",
                                Style.ALERT).show();
                    }

                }
            }).start();
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            if (placeHolderDrawable != null) {
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Handler for uncaught Exceptions
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable throwable) {
                throwable.printStackTrace();
            }
        });

        PhotoView photoView = new PhotoView(this);
        photoView.setMaximumScale(16);
        setContentView(photoView);

        Analytics.tagEvent(getApplicationContext(), "Bildactivity geöffnet");

        final ProgressDialog dlg = new ProgressDialog(this);
        dlg.setTitle("Bild wird geladen...");
        dlg.setIndeterminate(false);
        dlg.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dlg.show();

        extras = getIntent().getExtras();

        if (extras != null && extras.containsKey("imageUrl")) {

            url = extras.getString("imageUrl");

            Ion.with(this)
                    .load(url)
                    .progressDialog(dlg)
                    .withBitmap()
                    .deepZoom()
                    .intoImageView(photoView)
                    .setCallback(new FutureCallback<ImageView>() {
                        @Override
                        public void onCompleted(Exception e, ImageView result) {
                            dlg.cancel();
                        }
                    });

        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu resource file.
        getMenuInflater().inflate(R.menu.imageviewer, menu);

        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.menu_share);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getTitle().equals("teilen")) {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, extras.getString("imageUrl"));
            sendIntent.setType("text/plain");
            startActivity(sendIntent);

        } else if (item.getTitle().equals("download")) {

            Picasso.with(this)
                    .load(url)
                    .into(target);


        }

        return true;

    }

    @Override
    protected void onPause() {
        super.onPause();
        Tools.setIsVisible(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Tools.setIsVisible(true);

    }
}
