package de.allmystery.patric.allmyapp.helper;

import com.crashlytics.android.Crashlytics;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Layout;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.MotionEvent;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.allmystery.patric.allmyapp.activities.CategoryActivity;
import de.allmystery.patric.allmyapp.activities.ImageViewerActivity;
import de.allmystery.patric.allmyapp.activities.ProfilActivity;

public class CustomLinkMovementMethod extends LinkMovementMethod {

    private static Context movementContext;

    private static CustomLinkMovementMethod linkMovementMethod = new CustomLinkMovementMethod();

    public static android.text.method.MovementMethod getInstance(Context c) {

        movementContext = c;
        return linkMovementMethod;
    }

    public boolean onTouchEvent(android.widget.TextView widget, android.text.Spannable buffer,
            android.view.MotionEvent event) {
        int action = event.getAction();

        if (action == MotionEvent.ACTION_UP) {
            int x = (int) event.getX();
            int y = (int) event.getY();

            x -= widget.getTotalPaddingLeft();
            y -= widget.getTotalPaddingTop();

            x += widget.getScrollX();
            y += widget.getScrollY();

            Layout layout = widget.getLayout();
            int line = layout.getLineForVertical(y);
            int off = layout.getOffsetForHorizontal(line, x);

            URLSpan[] link = buffer.getSpans(off, off, URLSpan.class);
            if (link.length != 0) {
                String url = link[0].getURL();

                //imagelinks
                if (url.contains("ShowImage://")) {

                    Intent intent = new Intent(movementContext, ImageViewerActivity.class);
                    intent.putExtra("imageUrl", url.replace("ShowImage://", ""));
                    movementContext.startActivity(intent);

                    //youtubevideos
                } else if (url.contains("vnd.youtube://")) {

                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        movementContext.startActivity(intent);

                    } catch (ActivityNotFoundException e) {

                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                                url.replace("vnd.youtube://", "https://www.youtube.com/watch?v=")));
                        movementContext.startActivity(intent);
                    }

                    //allmystery links

                } else if (url.startsWith("/themen/") || url.startsWith("/tags/") || url
                        .startsWith("/blogs/") || url
                        .startsWith("http://www.allmystery.de/themen/")) {

                    Log.i("URL", url);
                    if (url.startsWith("/themen/") || url
                            .startsWith("http://www.allmystery.de/themen/")) {

                        Pattern p = Pattern.compile("/themen/(.{2})(\\d+)?-?(\\d+)?(#id)?(\\d+)?");
                        final Matcher m = p.matcher(url);

                        while (m.find()) {

                            Intent intent = new Intent(movementContext, CategoryActivity.class);

                            if (m.group(1) != null && m.group(2) == null) {

                                if (m.group(3) != null) {

                                    intent.putExtra("jumpTo",
                                            (Integer.parseInt(m.group(3)) * 35) / 25);

                                }

                                Tools.startCategory(m.group(1), movementContext, intent);

                            } else if (m.group(2) != null) {

                                if (m.group(3) != null) {

                                    intent.putExtra("jumpTo", Integer.parseInt(m.group(3)));

                                }

                                Tools.startThread(Integer.parseInt(m.group(2)), movementContext,
                                        intent);
                            }


                        }

                    }

                } else if (url.startsWith("/mitglieder/")) {

                    Intent intent = new Intent(movementContext, ProfilActivity.class);
                    intent.putExtra("userName", url.split("/")[2]);
                    movementContext.startActivity(intent);

                } else if (url.startsWith("/wiki")) {

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://allmystery.de" + url));
                    movementContext.startActivity(browserIntent);

                } else {

                    try {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        movementContext.startActivity(browserIntent);
                    } catch (ActivityNotFoundException e) {

                        Crashlytics.log(url);
                        Crashlytics.logException(e);
                    }


                }

                return true;
            }
        }

        return true;
        //return super.onTouchEvent(widget, buffer, event);
    }
}
