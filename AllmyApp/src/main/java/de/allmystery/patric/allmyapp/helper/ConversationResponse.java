package de.allmystery.patric.allmyapp.helper;

import java.util.List;

/**
 * Created by freg on 14.12.2014.
 */
public class ConversationResponse {

    private List<Conversation> conversations;
    private UserData userdata;

    public List<Conversation> getConversations() {
        return conversations;
    }

    public UserData getUserdata() {
        return userdata;
    }
}
