package de.allmystery.patric.allmyapp.helper;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.activities.CategoryActivity;
import de.allmystery.patric.allmyapp.activities.MainActivity;
import de.allmystery.patric.allmyapp.activities.PnActivity;
import de.allmystery.patric.allmyapp.events.NotificationEvent;
import de.greenrobot.event.EventBus;

/**
 * Created by freg on 14.11.2014.
 */
public class GcmIntentService extends IntentService {

    private NotificationManager mNotificationManager;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        final SharedPreferences prefs = getSharedPreferences("userdata", MODE_PRIVATE);

        if (!extras.isEmpty() && prefs.contains("token")) {

            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {

                sendNotification(extras);

            }
        }

        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification(Bundle extras) {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancelAll();

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this);

        mBuilder.setSmallIcon(R.drawable.ic_launcher)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true);

        if (sharedPref.contains("cancelNotificationOnReadInBrowser")
                && sharedPref.getBoolean("cancelNotificationOnReadInBrowser", false)
                && extras.getString("collapse_key").equals("quiet")) {
            mNotificationManager.cancel(1);
        }

        String type = extras.getString("collapse_key");

        if (!Tools.isVisible) {

            if (type.equals("pn")) {
                Tools.setPn(extras.getString("unreadCount"));
            }
            if (type.equals("bookmarks")) {
                Tools.addBookmark();
            }

            if ((Tools.getPn() + Tools.getBookmarks()) == 1) {

                if (type.equals("pn")) {

                    Intent answerPnIntent = new Intent(getApplicationContext(), PnActivity.class);

                    Conversation con = new Conversation(extras.getString("userid"),
                            extras.getString("username"));
                    answerPnIntent.setAction("PN ANTWORTEN");
                    answerPnIntent.putExtra("de.allmystery.patric.allmyapp.conversation", con);

                    Intent overViewPnIntent = new Intent(getApplicationContext(),
                            MainActivity.class);
                    overViewPnIntent.setAction("OVERVIEW PN");

                    PendingIntent pendingAnswerPnIntent = PendingIntent.getActivity(this, 0,
                            answerPnIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                    PendingIntent pendingOverviewPnIntent = PendingIntent.getActivity(this, 0,
                            overViewPnIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                    String title = "Neue Nachricht von " + extras.getString("username");

                    mBuilder.setTicker(title)
                            .setContentTitle(title)
                            .setStyle(new NotificationCompat.BigTextStyle()
                                    .bigText(extras.getString("message")))
                            .addAction(R.drawable.ic_fa_reply, "Antworten", pendingAnswerPnIntent)
                            .addAction(R.drawable.ic_action_nachrichten, "Übersicht",
                                    pendingOverviewPnIntent)

                            .setContentIntent(pendingOverviewPnIntent);

                } else if (type.equals("bookmarks")) {

                    Intent answerBookmarkIntent = new Intent(getApplicationContext(),
                            CategoryActivity.class);

                    answerBookmarkIntent.putExtra("de.allmystery.patric.allmyapp.newThread",
                            extras.getString("threadid"));
                    answerBookmarkIntent.setAction("SHOW THREAD");

                    Intent overViewBookmarkIntent = new Intent(getApplicationContext(),
                            MainActivity.class);
                    overViewBookmarkIntent.setAction("OVERVIEW BOOKMARKS");

                    PendingIntent pendingAnswerBookmarkIntent = PendingIntent.getActivity(this, 0,
                            answerBookmarkIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                    PendingIntent pendingOverviewBookmarkIntent = PendingIntent.getActivity(this, 0,
                            overViewBookmarkIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                    String title = extras.getString("message");

                    mBuilder.setTicker(title)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(title))
                            .addAction(R.drawable.ic_fa_reply, "Antworten",
                                    pendingAnswerBookmarkIntent)
                            .addAction(R.drawable.ic_action_nachrichten, "Übersicht",
                                    pendingOverviewBookmarkIntent)
                            .setContentIntent(pendingOverviewBookmarkIntent);

                }

            } else {

                Intent multiple = new Intent(getApplicationContext(),
                        MainActivity.class);

                PendingIntent pendingMultiple = PendingIntent.getActivity(this, 0,
                        multiple, PendingIntent.FLAG_UPDATE_CURRENT);

                mBuilder.setTicker((Tools.getPn() + Tools.getBookmarks()) + " neue Nachrichten")
                        .setContentTitle(
                                (Tools.getPn() + Tools.getBookmarks()) + " neue Nachrichten");

                StringBuilder builder = new StringBuilder();

                if (Tools.getPn() >= 1) {

                    builder.append(Tools.getPn())
                            .append(Tools.getPn() > 1 ? " neue Pns" : " neue Pn");

                    if (Tools.getBookmarks() >= 1) {

                        builder.append(" / ");
                    }

                }

                if (Tools.getBookmarks() >= 1) {

                    builder.append(Tools.getBookmarks())
                            .append(Tools.getBookmarks() > 1 ? " neue Beiträge" : " neuer Beitrag");
                }

                mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(builder.toString()))
                        .setContentIntent(pendingMultiple);

            }

            if (type.contains("bookmark")) {

                if (sharedPref.getBoolean("showBookmarks", true)) {
                    mNotificationManager.notify(1, mBuilder.build());
                }

            } else if (type.contains("pn")) {

                if (sharedPref.getBoolean("showPns", true)) {
                    mNotificationManager.notify(1, mBuilder.build());
                }
            }


        } else {
            EventBus.getDefault().post(new NotificationEvent());
        }

    }

}