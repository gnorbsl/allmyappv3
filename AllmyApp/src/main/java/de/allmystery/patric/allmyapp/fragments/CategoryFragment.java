package de.allmystery.patric.allmyapp.fragments;

import com.tjerkw.slideexpandable.library.ActionSlideExpandableListView;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.adapter.CategoryListAdapter;
import de.allmystery.patric.allmyapp.helper.AllmyApiClient;
import de.allmystery.patric.allmyapp.helper.Category;
import de.allmystery.patric.allmyapp.helper.CategoryResponse;
import de.allmystery.patric.allmyapp.helper.ThreadObject;
import de.allmystery.patric.allmyapp.helper.Tools;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Patric on 27.05.2014.
 */
public class CategoryFragment extends ListFragment {

    int pageNumber;

    Bundle extras;

    ActionSlideExpandableListView listView;

    String cat;

    private CategoryListAdapter adapter;

    private List<ThreadObject> threadAdapterList;

    private ProgressInterface progressInterface;

    public static CategoryFragment newInstance(int pageNumber) {

        CategoryFragment f = new CategoryFragment();

        Bundle args = new Bundle();
        args.putInt("pageNumber", pageNumber);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            progressInterface = (ProgressInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement Interface");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_thread, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Category category = null;

        Bundle args = getArguments();
        if (args != null) {
            pageNumber = args.getInt("pageNumber");

        }

        extras = getActivity().getIntent().getExtras();

        if (extras != null) {

            category = extras.getParcelable("de.allmystery.patric.allmyapp.category");

        }

        threadAdapterList = new ArrayList<>();
        adapter = new CategoryListAdapter(getActivity(), threadAdapterList);

        listView = (ActionSlideExpandableListView) getListView();

        listView.setAdapter(adapter);

        getCategory(category);

    }

    private void getCategory(Category category) {

        progressInterface.showProgressBar(true);

        AllmyApiClient.getAllmyApiInterface(getActivity()).getCategory(category.getCat(),
                pageNumber * 25, extras.getString("de.allmystery.patric.allmyapp.query"),
                new Callback<CategoryResponse>() {
                    @Override
                    public void success(CategoryResponse categoryResponse, Response response) {

                        if (categoryResponse != null) {

                            Log.i("CATEGORYFRAGMENT ", response.getUrl());

                            progressInterface.showProgressBar(false);

                            threadAdapterList.clear();
                            threadAdapterList.addAll(categoryResponse.getThreads());

                            adapter.notifyDataSetChanged();

                            Tools.handleUserdata(categoryResponse.getUserdata());
                        }


                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressInterface.showProgressBar(false);

                        if (error != null && getActivity() != null) {

                            Tools.handleError(getActivity(), error);

                        }
                    }
                });

    }

    public interface ProgressInterface {

        public void showProgressBar(boolean showProgressBar);
    }


}
