package de.allmystery.patric.allmyapp.helper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import com.crashlytics.android.Crashlytics;
import com.localytics.android.LocalyticsAmpSession;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.allmystery.patric.allmyapp.BuildConfig;
import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.activities.CategoryActivity;
import de.allmystery.patric.allmyapp.events.NewUserDataEvent;
import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Patric on 03.06.2014.
 */
public class Tools {

    public static final String PROPERTY_REG_ID = "registration_id";

    private static final Map<Pattern, Integer> emoticons = new HashMap<>();

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private static final String PROPERTY_APP_VERSION = "appVersion";

    public static String username;

    public static boolean isVisible;

    static String regid;

    static String SENDER_ID = "416789639491";

    static List<String> notifications;

    static {
        addPattern(":X", R.drawable.x);
        addPattern(":|", R.drawable.ratlos);
        addPattern(":)", R.drawable.lachen);
        addPattern(":(", R.drawable.traurig);
        addPattern(";)", R.drawable.spass);
        addPattern(":D", R.drawable.llachen);
        addPattern(":P", R.drawable.tongue2);
        addPattern(":alien:", R.drawable.alien);
        addPattern(":}", R.drawable.teufel);
        addPattern(":{", R.drawable.kotz);
        addPattern(":cool:", R.drawable.cool);
        addPattern(":fuya:", R.drawable.fuya);
        addPattern(":kiff:", R.drawable.kiffen);
        addPattern(":no:", R.drawable.wrong);
        addPattern(":ok:", R.drawable.ok);
        addPattern(":bier:", R.drawable.beer);
        addPattern(":ask:", R.drawable.ask);
        addPattern(":shot:", R.drawable.shot);
        addPattern(":nessie:", R.drawable.nessie);
        addPattern(":ghost:", R.drawable.ghost);
        addPattern(":N:", R.drawable.thumbs_down);
        addPattern(":Y:", R.drawable.thumbs_up);
        addPattern(":merle:", R.drawable.merle);
        addPattern(":tannenbaum:", R.drawable.tannenbaum);
        addPattern(":dog:", R.drawable.dog);
        addPattern(":vodka:", R.drawable.vodka);
        addPattern(":nerv:", R.drawable.nerv);
        addPattern(":mexican:", R.drawable.mexican);
        addPattern(":lv:", R.drawable.love);
        addPattern(":troll:", R.drawable.trollface);
        addPattern(":santa:", R.drawable.santa);
        addPattern(":o:", R.drawable.oo);
        addPattern(":hombre:", R.drawable.hombre);
        addPattern(":fuyeah:", R.drawable.fuyeah);
        addPattern(":engel:", R.drawable.engel);
        addPattern(":wein:", R.drawable.wein);
        addPattern(":mozart:", R.drawable.mozart);
        addPattern(":pic:", R.drawable.pic);
        addPattern(":trollbier:", R.drawable.trollbier);
        addPattern(":popcorn:", R.drawable.popcorn);
        addPattern(":trollking:", R.drawable.trollking);
        addPattern(":kingfu:", R.drawable.kingfu);
        addPattern(":tv:", R.drawable.tv);
        addPattern(":kc:", R.drawable.kc);
        addPattern(":wicht:", R.drawable.link);
        addPattern(":sleepy:", R.drawable.sleepy);
        addPattern(":kaffee:", R.drawable.kaffee);
        addPattern(":note:", R.drawable.note);
        addPattern(":allmy:", R.drawable.allmy_icon_tiny);
        addPattern(":cry:", R.drawable.cry);
        addPattern(":datrueffel:", R.drawable.datrueffel);
        addPattern(":target:", R.drawable.target);
        addPattern(":mosquito:", R.drawable.mosquito);
        addPattern(":chatter:", R.drawable.chatter);
        addPattern(":rock:", R.drawable.rock);
        addPattern(":vv:", R.drawable.vv);
        addPattern(":cat:", R.drawable.cat);
        addPattern(":mouse:", R.drawable.mouse);
        addPattern(":king:", R.drawable.king);
        addPattern(":chinamann:", R.drawable.chinamann);
        addPattern(":dino:", R.drawable.dino);
        addPattern(":krbis:", R.drawable.krbis);
        addPattern(":trollsanta:", R.drawable.trollsanta);
        addPattern(":ganja:", R.drawable.ganja);
        addPattern(":blackmetal:", R.drawable.blackmetal);
        addPattern(":trashmetal:", R.drawable.trashmetal);
        addPattern(":doommetal:", R.drawable.doommetal);
        addPattern(":deathmetal:", R.drawable.deathmetal);
        addPattern(":folkmetal:", R.drawable.folkmetal);
        addPattern(":powermetal:", R.drawable.powermetal);
    }

    static boolean hasLargeScreen;

    static Context localContext;

    static Tools tools;

    private static LocalyticsAmpSession localytocsSession;

    private static GoogleCloudMessaging gcm;

    private static int pn = 0;

    private static int bookmark = 0;

    private static boolean reRegistration;


    private Tools(Activity categoryActivity) {
        localContext = categoryActivity;

    }

    private static void addPattern(String smile,
            int resource) {
        emoticons.put(Pattern.compile(Pattern.quote(smile)), resource);
    }

    public static Map<Pattern, Integer> getSmileyMap() {
        return emoticons;
    }

    public static SpannableString addSmileys(Context context, SpannableString postText) {

        Map<Pattern, Integer> emoticons = Tools.getSmileyMap();

        for (Map.Entry<Pattern, Integer> entry : emoticons.entrySet()) {
            Matcher matcher = entry.getKey().matcher(postText);
            while (matcher.find()) {
                boolean set = true;
                for (ImageSpan span : postText.getSpans(matcher.start(),
                        matcher.end(), ImageSpan.class)) {
                    if (postText.getSpanStart(span) >= matcher.start()
                            && postText.getSpanEnd(span) <= matcher.end()) {
                        postText.removeSpan(span);
                    } else {
                        set = false;
                        break;
                    }
                }
                if (set) {

                    postText.setSpan(new ImageSpan(context, entry.getValue()),
                            matcher.start(), matcher.end(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }

        return postText;
    }

    public static Tools getInstance(Activity activity) {

        if (tools != null) {
            setActivity(activity);
            return tools;
        } else {
            tools = new Tools(activity);
            return tools;
        }

    }

    public static void setActivity(Activity activity) {
        localContext = activity;
    }

    public static String convertTimeString(String date) {

        DateTimeFormatter fmt = DateTimeFormat.forPattern("EEE dd MMM yyyy HH:mm:ss Z")
                .withLocale(Locale.ENGLISH);
        DateTime postTime = fmt.parseDateTime(date);

        DateTime now = DateTime.now();

        Seconds secondsBetween = Seconds.secondsBetween(postTime, now);
        Minutes minutesBetween = Minutes.minutesBetween(postTime, now);
        Hours hoursBetween = Hours.hoursBetween(postTime, now);
        Days daysBetween = Days.daysBetween(postTime, now);

        int seconds = secondsBetween.getSeconds();
        //TODO fix bug with negative seconds, maybe only an emulator issue
        int minutes = minutesBetween.getMinutes();
        int hours = hoursBetween.getHours();
        int days = daysBetween.getDays();

        String timeString;

        if (seconds > 60) {

            if (minutes > 60) {

                if (hours > 24) {

                    timeString = "am " + postTime.getDayOfMonth() + "." + postTime.getMonthOfYear()
                            + "." + postTime.getYear() + " um " + (postTime.getHourOfDay() < 10 ?
                            "0" + postTime.getHourOfDay() : postTime.getHourOfDay())
                            + ":" + (postTime.getMinuteOfHour() < 10 ? "0" + postTime
                            .getMinuteOfHour() : postTime.getMinuteOfHour());

                } else {

                    if (hours == 1) {

                        timeString = "vor " + hours + " Stunde";
                    } else {

                        timeString = "vor " + hours + " Stunden";
                    }


                }

            } else {

                if (minutes == 1) {

                    timeString = "vor " + minutes + " Minute";
                } else {

                    timeString = "vor " + minutes + " Minuten";
                }


            }

        } else {

            if (seconds == 1) {

                timeString = "vor " + seconds + " Sekunde";
            } else {

                timeString = "vor " + seconds + " Sekunden";
            }
        }

        return timeString;

    }

    public static void setHasLargeScreen(boolean hasLargeScreen) {
        Tools.hasLargeScreen = hasLargeScreen;
    }

    public static boolean hasLargeScreen() {
        return hasLargeScreen;
    }

    public static int getOrientation() {

        return localContext.getResources().getConfiguration().orientation;

    }


    public static void setIsVisible(boolean visible) {

        Tools.isVisible = visible;

    }

    public static void getUserData(final Activity activity) {

        AllmyApiClient.getAllmyApiInterface(activity).getUserData(new Callback<UserDataResponse>() {
            @Override
            public void success(UserDataResponse userDataResponse, Response response) {

                handleUserdata(userDataResponse.getUserdata());

            }

            @Override
            public void failure(RetrofitError error) {

                Tools.handleError(activity, error);
            }
        });


    }

    public static void handleUserdata(UserData userdata) {

        if (userdata != null) {

            EventBus.getDefault().removeAllStickyEvents();

            Crashlytics.setUserName(userdata.getUsername());
            Crashlytics.setUserIdentifier(userdata.getUsername());
            Tools.username = userdata.getUsername();

            int newBookmarks = userdata.getUnread_bookmarks();
            int newMessage = userdata.getUnread_messages();

            EventBus.getDefault().postSticky(new NewUserDataEvent(newMessage, newBookmarks));

        }

    }

    public static void showBookmarks(final Activity activity) {

        final Category category = new Category();
        category.setTitle_short("Beobachtet");
        category.setCat("bookmarks");

        AllmyApiClient.getAllmyApiInterface(activity).getCategory("bookmarks",
                new Callback<CategoryResponse>() {
                    @Override
                    public void success(CategoryResponse categoryResponse,
                            Response response) {

                        Intent intent = new Intent(activity,
                                CategoryActivity.class);

                        category.setThreads_count(categoryResponse.getCount());

                        intent.putExtra("de.allmystery.patric.allmyapp.category", category);
                        activity.startActivity(intent);
                        new ActivityAnimator().fadeAnimation(activity);

                    }

                    @Override
                    public void failure(RetrofitError error) {

                        Tools.handleError(activity, error);
                    }
                });


    }

    public static void addThread(int thread_id, final Activity activity) {

        AllmyApiClient.getAllmyApiInterface(activity)
                .watchThread(new WatchThread(thread_id), new Callback<WatchThreadResponse>() {
                    @Override
                    public void success(WatchThreadResponse watchThreadResponse,
                            Response response) {

                        if (watchThreadResponse.isSuccess()) {

                            Crouton.makeText(activity,
                                    activity.getString(R.string.thread_watched),
                                    Style.INFO).show();

                        } else {

                            Crouton.makeText(activity,
                                    activity.getString(R.string.thread_watched_error) +
                                            watchThreadResponse.getError(),
                                    Style.ALERT).show();

                        }


                    }

                    @Override
                    public void failure(RetrofitError error) {

                        Tools.handleError(activity, error);

                    }
                });

    }

    public static void removeThread(int thread_id, final Activity activity) {

        AllmyApiClient.getAllmyApiInterface(activity).unWatchThread(new UnWatchThread(thread_id),
                new Callback<WatchThreadResponse>() {
                    @Override
                    public void success(WatchThreadResponse watchThreadResponse,
                            Response response) {

                        if (watchThreadResponse.isSuccess()) {

                            Crouton.makeText(activity,
                                    activity.getString(R.string.thread_removed),
                                    Style.INFO).show();

                        } else {

                            Crouton.makeText(activity,
                                    activity.getString(R.string.thread_removed_error) +
                                            watchThreadResponse.getError(),
                                    Style.ALERT).show();

                        }


                    }

                    @Override
                    public void failure(RetrofitError error) {

                        Tools.handleError(activity, error);

                    }
                });

    }

    public static void showNewDiscussions(final FragmentActivity activity) {

        final Category category = new Category();
        category.setTitle_short("Neue Diskussionen");
        category.setCat("new");

        AllmyApiClient.getAllmyApiInterface(activity).getCategory("new",
                new Callback<CategoryResponse>() {
                    @Override
                    public void success(CategoryResponse categoryResponse,
                            Response response) {

                        Intent intent = new Intent(activity,
                                CategoryActivity.class);

                        category.setThreads_count(categoryResponse.getCount());

                        intent.putExtra("de.allmystery.patric.allmyapp.category", category);
                        activity.startActivity(intent);
                        new ActivityAnimator().fadeAnimation(activity);

                    }

                    @Override
                    public void failure(RetrofitError error) {

                        Tools.handleError(activity, error);
                    }
                });


    }


    //start the categoryactivity and load pagenumber and title to create a categoryobject
    //also add additional GET parameters if neccesary
    public static void startCategory(String cat, final Context context, final Intent intent) {

        final Category category = new Category();

        if (!intent.hasExtra("de.allmystery.patric.allmyapp.query")) {

            SharedPreferences cats = context.getSharedPreferences(cat, Context.MODE_PRIVATE);
            category.setTitle_short(cats.getString("title_short", ""));

        } else {
            category.setTitle_short("Suchergebnisse:");
        }

        category.setCat(cat);

        AllmyApiClient.getAllmyApiInterface(context)
                .getCategory(cat, intent.getStringExtra("de.allmystery.patric.allmyapp.query"),
                        new Callback<CategoryResponse>() {
                            @Override
                            public void success(CategoryResponse categoryResponse,
                                    Response response) {

                                category.setThreads_count(categoryResponse.getCount());

                                intent.putExtra("de.allmystery.patric.allmyapp.category", category);
                                context.startActivity(intent);

                            }

                            @Override
                            public void failure(RetrofitError error) {

                                Tools.handleError(context, error);

                            }
                        });


    }

    public static void startThread(final int threadId, final Context context, final Intent intent) {

        AllmyApiClient.getAllmyApiInterface(context)
                .getThread(threadId, 0, new Callback<PostResponse>() {
                    @Override
                    public void success(PostResponse postResponse, Response response) {

                        ThreadObject thread = new ThreadObject();
                        thread.setThread_id(threadId);
                        thread.setClosed(postResponse.isThread_closed());
                        thread.setPages(postResponse.getThread_pages());
                        thread.setTitle(postResponse.getThread_title());
                        thread.setCategory(postResponse.getThread_category());

                        intent.putExtra("de.allmystery.patric.allmyapp.thread", thread);
                        context.startActivity(intent);
                    }

                    @Override
                    public void failure(RetrofitError error) {

                        Tools.handleError(context, error);


                    }
                });


    }

    public static void handleError(Context context, RetrofitError error) {

        if (context != null && error != null) {

            if (error.getKind() == RetrofitError.Kind.NETWORK
                    || error.getKind() == RetrofitError.Kind.HTTP) {

                Crouton.makeText((Activity) context,
                        "Netzwerkfehler, Internetverbindung verfügbar?",
                        Style.ALERT).show();

            } else {

                Crashlytics.logException(error);
                Crouton.makeText((Activity) context,
                        "Ein Fehler trat auf, der Entwickler wurde benachrichtigt",
                        Style.ALERT).show();

            }
        }


    }


    public static String getUsername(Context context) {

        SharedPreferences pref = context.getSharedPreferences("userdata", Context.MODE_PRIVATE);

        return pref.getString("username", "N/A");
    }

    public static void reRegisterGCM(Context context) {

        reRegistration = true;
        final SharedPreferences prefs = getGCMPreferences(context);
        prefs.edit().remove(PROPERTY_REG_ID).apply();

        registerGCM(context);
    }

    public static boolean registerGCM(Context context) {

        if (checkPlayServices(context)) {

            gcm = GoogleCloudMessaging.getInstance(context);

            regid = getRegistrationId(context);
            Log.i("GCM", "check successful " + regid);
            if (regid.isEmpty()) {
                registerInBackground(context);
            } else {
                sendRegistrationIdToBackend(regid, context);
            }


        }

        return false;
    }

    private static boolean checkPlayServices(Context context) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) context,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {

                //finish();
            }
            return false;
        }
        return true;
    }


    private static String getRegistrationId(Context context) {

        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        String registeredVersion = prefs.getString(PROPERTY_APP_VERSION, "");
        String currentVersion = BuildConfig.VERSION_NAME;
        if (!registeredVersion.equals(currentVersion)) {
            return registrationId;
        }
        return registrationId;
    }

    private static SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return context.getSharedPreferences(Tools.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private static void registerInBackground(final Context context) {
        new AsyncTask<String, String, String>() {

            @Override
            protected String doInBackground(String... strings) {

                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid + "%%%";

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.
                    sendRegistrationIdToBackend(regid, context);

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(context, regid);


                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }

                return msg;
            }

            @Override
            protected void onPostExecute(String s) {

                super.onPostExecute(s);

            }
        }.execute(null, null, null);

    }


    private static void sendRegistrationIdToBackend(String regId, final Context context) {

        AllmyApiClient.getAllmyApiInterface(context).registerGcm(new Token(regId),
                new Callback<TokenResponse>() {
                    @Override
                    public void success(TokenResponse tokenResponse, Response response) {

                        if (reRegistration) {
                            Crouton.makeText((Activity) context,
                                    "Registrierung erfolgreich",
                                    Style.INFO).show();
                            reRegistration = false;
                        }


                    }

                    @Override
                    public void failure(RetrofitError error) {

                        Tools.handleError(context, error);
                    }
                });

    }

    private static void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String appVersion = BuildConfig.VERSION_NAME;
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putString(PROPERTY_APP_VERSION, appVersion);
        editor.apply();
    }


    public static void addNotification(String title) {

        if (notifications != null) {

            notifications.add(title);
        } else {
            notifications = new ArrayList<>();
            notifications.add(title);
        }


    }

    public static List<String> getNotifications() {
        return notifications;
    }

    public static void addPn() {

        pn++;

    }

    public static void addBookmark() {

        bookmark++;

    }

    public static int getPn() {
        return pn;
    }

    public static void setPn(String pn) {
        Tools.pn = Integer.parseInt(pn);
    }

    public static int getBookmarks() {
        return bookmark;
    }

    public static void clearNotifications() {

        pn = 0;
        bookmark = 0;

    }
}
