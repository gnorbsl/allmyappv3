package de.allmystery.patric.allmyapp.helper;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by freg on 14.12.2014.
 */
public class Category implements Parcelable {


    private String cat;
    private String title_long;
    private String title_short;
    private String pic;
    private int last_post_threadid;
    private String last_post_title;
    private String last_post_date;
    private int last_post_page;
    private int count;
    private int threads_count;
    private int last_post_id;
    private String last_post_username;
    private String created_username;
    private String default_tags;


    public void setPic(String pic) {
        this.pic = pic;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public void setTitle_long(String title_long) {
        this.title_long = title_long;
    }

    public void setTitle_short(String title_short) {
        this.title_short = title_short;
    }

    public void setLast_post_threadid(int last_post_threadid) {
        this.last_post_threadid = last_post_threadid;
    }

    public void setLast_post_title(String last_post_title) {
        this.last_post_title = last_post_title;
    }

    public void setLast_post_date(String last_post_date) {
        this.last_post_date = last_post_date;
    }

    public void setLast_post_page(int last_post_page) {
        this.last_post_page = last_post_page;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setThreads_count(int threads_count) {
        this.threads_count = threads_count;
    }

    public void setLast_post_id(int last_post_id) {
        this.last_post_id = last_post_id;
    }

    public void setLast_post_username(String last_post_username) {
        this.last_post_username = last_post_username;
    }

    public void setCreated_username(String created_username) {
        this.created_username = created_username;
    }

    public void setDefault_tags(String default_tags) {
        this.default_tags = default_tags;
    }



    public String getCat() {
        return cat;
    }

    public String getTitle_long() {
        return title_long;
    }

    public String getTitle_short() {
        return title_short;
    }

    public String getPic() {
        return pic;
    }

    public int getLast_post_threadid() {
        return last_post_threadid;
    }

    public String getLast_post_title() {
        return last_post_title;
    }

    public String getLast_post_date() {
        return Tools.convertTimeString(last_post_date);
    }

    public int getLast_post_page() {
        return last_post_page;
    }

    public int getCount() {
        return count;
    }

    public int getThreads_count() {
        return threads_count;
    }

    public int getLast_post_id() {
        return last_post_id;
    }

    public String getLast_post_username() {
        return last_post_username;
    }

    public String getCreated_username() {
        return created_username;
    }

    public String getDefault_tags() {
        return default_tags;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.cat);
        dest.writeString(this.title_long);
        dest.writeString(this.title_short);
        dest.writeString(this.pic);
        dest.writeInt(this.last_post_threadid);
        dest.writeString(this.last_post_title);
        dest.writeString(this.last_post_date);
        dest.writeInt(this.last_post_page);
        dest.writeInt(this.count);
        dest.writeInt(this.threads_count);
        dest.writeInt(this.last_post_id);
        dest.writeString(this.last_post_username);
        dest.writeString(this.created_username);
        dest.writeString(this.default_tags);
    }

    public Category() {
    }

    private Category(Parcel in) {
        this.cat = in.readString();
        this.title_long = in.readString();
        this.title_short = in.readString();
        this.pic = in.readString();
        this.last_post_threadid = in.readInt();
        this.last_post_title = in.readString();
        this.last_post_date = in.readString();
        this.last_post_page = in.readInt();
        this.count = in.readInt();
        this.threads_count = in.readInt();
        this.last_post_id = in.readInt();
        this.last_post_username = in.readString();
        this.created_username = in.readString();
        this.default_tags = in.readString();
    }

    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
