package de.allmystery.patric.allmyapp.helper;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by freg on 15.12.2014.
 */
public class Post implements Parcelable {

    private int userId;
    private String date;
    private String text;
    private int post_id;
    private String usercolor;
    private String pic;
    private String username;

    public int getUserId() {
        return userId;
    }

    public String getDate() {
        return Tools.convertTimeString(date);
    }

    public String getText() {
        return text;
    }

    public int getPost_id() {
        return post_id;
    }

    public String getUsercolor() {
        return usercolor;
    }

    public String getPic() {
        return pic;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.userId);
        dest.writeString(this.date);
        dest.writeString(this.text);
        dest.writeInt(this.post_id);
        dest.writeString(this.usercolor);
        dest.writeString(this.pic);
        dest.writeString(this.username);
    }

    public Post() {
    }

    private Post(Parcel in) {
        this.userId = in.readInt();
        this.date = in.readString();
        this.text = in.readString();
        this.post_id = in.readInt();
        this.usercolor = in.readString();
        this.pic = in.readString();
        this.username = in.readString();
    }

    public static final Parcelable.Creator<Post> CREATOR = new Parcelable.Creator<Post>() {
        public Post createFromParcel(Parcel source) {
            return new Post(source);
        }

        public Post[] newArray(int size) {
            return new Post[size];
        }
    };

    public void setText(String text) {
        this.text = text;
    }
}
