package de.allmystery.patric.allmyapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import de.allmystery.patric.allmyapp.R;

/**
 * Created by Patric on 27.05.2014.
 */
public class MainActivityMenuAdapter extends BaseAdapter {


    Context context;

    List<String> items;

    View rowView;


    public MainActivityMenuAdapter(Context context, List<String> items) {

        this.context = context;
        this.items = items;


    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        final String item = items.get(i);

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(context);

            if (item.equals("Navigation")) {

                view = inflater.inflate(R.layout.sectionmenu, viewGroup, false);

                TextView section = (TextView) view.findViewById(R.id.section);
                section.setCompoundDrawablesWithIntrinsicBounds(context.getResources()
                        .getIdentifier("ic_action_section", "drawable",
                                "de.allmystery.patric.allmyapp"), 0, 0, 0);
                section.setText(item);


            } else {

                view = inflater.inflate(R.layout.activity_main_list_rowview, viewGroup, false);

                TextView test = (TextView) view.findViewById(R.id.username);

                test.setCompoundDrawablesWithIntrinsicBounds(context.getResources()
                        .getIdentifier("ic_action_" + item.toLowerCase(), "drawable",
                                "de.allmystery.patric.allmyapp"), 0, 0, 0);
                test.setText(item);

            }


        }

        return view;
    }


}
