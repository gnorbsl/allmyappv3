package de.allmystery.patric.allmyapp.helper;

/**
 * Created by freg on 15.12.2014.
 */
public class SendPnObject {

    private String message;

    public SendPnObject(String message) {

        setMessage(message);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
