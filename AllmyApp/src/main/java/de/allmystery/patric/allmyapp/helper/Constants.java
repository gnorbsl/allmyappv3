package de.allmystery.patric.allmyapp.helper;

import android.content.Context;

import de.allmystery.patric.allmyapp.BuildConfig;

/**
 * Created by freg on 07.11.2014.
 */
public class Constants {

    public final static String USERAGENT = "Allmystery " + BuildConfig.VERSION_NAME + " ("
            + android.os.Build.MODEL + "; Android Version: " + android.os.Build.VERSION.SDK_INT
            + "; de_DE)";

    private static String TOKEN;

    public static String getTOKEN(Context context) {



        return context.getSharedPreferences("userdata", Context.MODE_PRIVATE)
                .getString("token", "");

    }


    public static void setToken(String TOKEN) {
        Constants.TOKEN = TOKEN;
    }
}



