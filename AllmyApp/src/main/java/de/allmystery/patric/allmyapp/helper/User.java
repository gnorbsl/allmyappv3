package de.allmystery.patric.allmyapp.helper;

/**
 * Created by freg on 14.12.2014.
 */
public class User {

    private String username;
    private String password;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

}
