package de.allmystery.patric.allmyapp.events;

/**
 * Created by freg on 08.11.2014.
 */
public class NewUserDataEvent {

    private int newMessage;

    private int newBookmark;

    public NewUserDataEvent(int newMessage, int newBookmarks) {

        this.newMessage = newMessage;
        this.newBookmark = newBookmarks;

    }

    public int getNewMessage() {
        return newMessage;
    }

    public int getNewBookmark() {
        return newBookmark;
    }
}
