package de.allmystery.patric.allmyapp.helper;

/**
 * Created by freg on 15.12.2014.
 */
public class WatchThread {

    private int add_thread;

    public int getAdd_thread() {
        return add_thread;
    }

    public void setAdd_thread(int add_thread) {
        this.add_thread = add_thread;
    }

    public WatchThread(int add_thread) {
        setAdd_thread(add_thread);
    }

}
