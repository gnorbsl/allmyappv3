package de.allmystery.patric.allmyapp.helper;

/**
 * Created by freg on 15.12.2014.
 */
public class SendPost {

    private String text;

    private String title;

    private String cat;

    public SendPost(String text) {
        setText(text);
    }

    public SendPost(String text, String title, String cat) {
        this(text);
        setTitle(title);
        setCat(cat);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }
}
