package de.allmystery.patric.allmyapp.helper;

/**
 * Created by freg on 15.12.2014.
 */
public class WatchThreadResponse {

    private boolean success;
    private String error;
    private UserData userdata;

    public boolean isSuccess() {
        return success;
    }

    public UserData getUserdata() {
        return userdata;
    }

    public String getError() {
        return error;
    }
}
