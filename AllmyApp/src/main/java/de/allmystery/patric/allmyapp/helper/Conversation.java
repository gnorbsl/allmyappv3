package de.allmystery.patric.allmyapp.helper;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * Created by freg on 14.12.2014.
 */
public class Conversation implements Parcelable {

    public static final Creator<Conversation> CREATOR = new Creator<Conversation>() {
        public Conversation createFromParcel(Parcel source) {
            return new Conversation(source);
        }

        public Conversation[] newArray(int size) {
            return new Conversation[size];
        }
    };

    private String username;

    private int userid;

    private String date;

    private String pic;

    private int unread_count;

    public Conversation(String userId, String username) {

        setUserid(Integer.parseInt(userId));
        Log.i("USERNBAME", username + "");
        setUsername(username);
    }

    public Conversation() {
    }

    private Conversation(Parcel in) {
        this.username = in.readString();
        this.userid = in.readInt();
        this.date = in.readString();
        this.pic = in.readString();
        this.unread_count = in.readInt();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getDate() {
        return Tools.convertTimeString(date);
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getUnread_count() {
        return unread_count;
    }

    public void setUnread_count(int unread_count) {
        this.unread_count = unread_count;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.username);
        dest.writeInt(this.userid);
        dest.writeString(this.date);
        dest.writeString(this.pic);
        dest.writeInt(this.unread_count);
    }
}
