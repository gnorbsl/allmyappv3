package de.allmystery.patric.allmyapp.helper;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by freg on 14.12.2014.
 */
public class FriendListResponse {

    @SerializedName("users")
    private List<Friend> friends;
    private UserData userdata;

    public List<Friend> getFriends() {
        return friends;
    }

    public UserData getUserdata() {
        return userdata;
    }
}
