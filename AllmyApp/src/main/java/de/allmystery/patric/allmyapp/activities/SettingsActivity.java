package de.allmystery.patric.allmyapp.activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.events.QuitEvent;
import de.allmystery.patric.allmyapp.helper.Tools;
import de.greenrobot.event.EventBus;

/**
 * Created by Patric on 01.06.2014.
 */
public class SettingsActivity extends Activity {

    TextView restart;

    TextView quit;

    TextView logout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_setting);

        restart = (TextView) findViewById(R.id.restart);
        quit = (TextView) findViewById(R.id.quit);
        logout = (TextView) findViewById(R.id.logout);

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                restart();
            }
        });

        quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);

                builder.setMessage("App beenden?");
                builder.setCancelable(true);
                builder.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        EventBus.getDefault().post(new QuitEvent());
                        finish();
                    }
                });
                builder.setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getSharedPreferences("token", MODE_PRIVATE);
                if (pref.contains("token")) {

                    SharedPreferences.Editor edit = pref.edit();
                    edit.clear().commit();
                    restart();
                }
            }
        });


    }


    private void restart() {

        Intent mStartActivity = new Intent(SettingsActivity.this, LoginActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent
                .getActivity(SettingsActivity.this, mPendingIntentId, mStartActivity,
                        PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) SettingsActivity.this
                .getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 400, mPendingIntent);
        System.exit(0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Tools.setIsVisible(false);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Tools.setIsVisible(true);

    }
}
