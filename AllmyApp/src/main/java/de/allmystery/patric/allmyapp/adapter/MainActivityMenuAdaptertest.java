package de.allmystery.patric.allmyapp.adapter;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.allmystery.patric.allmyapp.R;

/**
 * Created by Patric on 27.05.2014.
 */
public class MainActivityMenuAdaptertest extends BaseAdapter {

    Context context;

    List<String> items;

    View rowView;

    SpannableString bla;

    private ArrayList<CustomTarget> targetList;

    private ArrayList<FoundImage> foundImage;

    public MainActivityMenuAdaptertest(Context context, List<String> items) {

        this.context = context;
        this.items = items;

        targetList = new ArrayList<>();

    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        foundImage = new ArrayList<>();

        bla = new SpannableString("");

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.activity_main_list_rowview, viewGroup, false);

        }

        // Direct use of Pattern:
        Pattern smileyPattern = Pattern.compile("<span.*?url\\((.*?)\\).*?<\\/span>");
        Matcher smileyMatcher = smileyPattern.matcher(bla);
        while (smileyMatcher.find()) {

            FoundImage image = new FoundImage(smileyMatcher.group(1), smileyMatcher.start(),
                    smileyMatcher.end());
            foundImage.add(image);

        }

        Pattern imagePattern = Pattern.compile("<span.*?url\\((.*?)\\).*?<\\/span>");
        Matcher imageMatcher = imagePattern.matcher(bla);
        while (imageMatcher.find()) {

            FoundImage image = new FoundImage(imageMatcher.group(1), imageMatcher.start(),
                    imageMatcher.end());
            foundImage.add(image);

        }

        final LinearLayout linlayout = (LinearLayout) view.findViewById(R.id.menutest);

        final String item = items.get(i);
        final TextView test = (TextView) view.findViewById(R.id.username);

        Log.d("PICASSO", "start");

        for (FoundImage image : foundImage) {

            CustomTarget target = new CustomTarget(image, test);
            targetList.add(target);
            Picasso.with(context).load(image.url).into(target);

        }

        return view;
    }


    public class CustomTarget implements Target {

        FoundImage image;

        TextView test;

        public CustomTarget(FoundImage image, TextView test) {

            this.image = image;
            this.test = test;
        }


        @Override
        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {

            Log.d("TARGET", "Bitmap loaded");
            BitmapDrawable d = new BitmapDrawable(context.getResources(), bitmap);
            d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
            ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
            bla.setSpan(span, image.start, image.end, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

            test.setText(bla);
            Log.d("TARGET", bla.toString());
            releaseTargetReference();
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }


        private void releaseTargetReference() {

            targetList.remove(this);
        }

    }


    private class FoundImage {

        public String url;

        public int start;

        public int end;

        public FoundImage(String url, int start, int end) {

            this.url = url;
            this.start = start;
            this.end = end;

        }

    }
}
