package de.allmystery.patric.allmyapp.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.activities.AnswerActivity;
import de.allmystery.patric.allmyapp.activities.CategoryActivity;
import de.allmystery.patric.allmyapp.helper.ThreadObject;
import de.allmystery.patric.allmyapp.helper.Tools;

/**
 * Created by Patric on 27.05.2014.
 */
public class MoreFragment extends android.support.v4.app.Fragment {

    TextView newDiscussions;

    LinearLayout searchDiscussionLayout;

    TextView createDiscussion;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_more, container, false);

        newDiscussions = (TextView) view.findViewById(R.id.newDiscussion);
        searchDiscussionLayout = (LinearLayout) view.findViewById(R.id.searchDiscussionLayout);
        createDiscussion = (TextView) view.findViewById(R.id.createDiscussion);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        newDiscussions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Tools.showNewDiscussions(getActivity());

            }
        });

        searchDiscussionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());

                dialogBuilder.setTitle("Wonach soll gesucht werden?");

                final EditText searchQuery = new EditText(getActivity());

                dialogBuilder.setView(searchQuery);

                dialogBuilder.setPositiveButton("Los!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Intent intent = new Intent(getActivity(), CategoryActivity.class);
                        intent.putExtra("de.allmystery.patric.allmyapp.query",
                                searchQuery.getText().toString());

                        Tools.startCategory("search", getActivity(), intent);

                    }
                });

                dialogBuilder.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                dialogBuilder.create().show();

            }
        });

        createDiscussion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ThreadObject thread = new ThreadObject();
                thread.setThread_id(1);

                Intent intent = new Intent(getActivity(), AnswerActivity.class);
                intent.putExtra("de.allmystery.patric.allmyapp.thread", thread);
                intent.putExtra("de.allmystery.patric.allmyapp.newThread", true);

                getActivity().startActivity(intent);


            }
        });


    }
}
