package de.allmystery.patric.allmyapp.helper;

/**
 * Created by freg on 15.12.2014.
 */
public class SendPnResponse {

    private String message;
    private String error;
    private UserData UserData;

    public UserData getUserData() {
        return UserData;
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }
}
