package de.allmystery.patric.allmyapp.helper;

import com.localytics.android.LocalyticsActivityLifecycleCallbacks;
import com.localytics.android.LocalyticsAmpSession;

import android.app.Activity;
import android.content.Context;
import android.os.Build;

import java.util.Map;

/**
 * Created by freg on 11.01.2015.
 */
public class Analytics {

    private static LocalyticsAmpSession localytocsSession;

    public static void register(Activity activity) {

        if (isAboveApi14()) {

            activity.getApplication().registerActivityLifecycleCallbacks(
                    new LocalyticsActivityLifecycleCallbacks(getLocalytocsSession(activity)));

        }

    }

    private static boolean isAboveApi14() {

        return (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH);

    }

    public static LocalyticsAmpSession getLocalytocsSession(Context context) {

        if (localytocsSession == null) {
            localytocsSession = new LocalyticsAmpSession(context);
        }

        return localytocsSession;

    }


    public static void setName(Context context, String username) {

        if (isAboveApi14()) {
            getLocalytocsSession(context).setCustomerName(username);
        }


    }

    public static void tagEvent(Context context, String eventName) {

        if (isAboveApi14()) {
            getLocalytocsSession(context).tagEvent(eventName);
        }

    }

    public static void tagEvent(Context context, String eventName, Map<String, String> params) {

        if (isAboveApi14()) {
            getLocalytocsSession(context).tagEvent(eventName, params);
        }

    }

    public static void tagScreen(Context context, String screenName) {

        if (isAboveApi14()) {
            getLocalytocsSession(context).tagScreen(screenName);
        }

    }

    public static void upload(Context context) {

        if (isAboveApi14()) {
            getLocalytocsSession(context).upload();
        }


    }
}
