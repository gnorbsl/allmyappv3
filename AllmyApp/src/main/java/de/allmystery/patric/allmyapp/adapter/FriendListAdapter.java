package de.allmystery.patric.allmyapp.adapter;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.helper.Friend;

/**
 * Created by Patric on 29.05.2014.
 */
public class FriendListAdapter extends ArrayAdapter<Friend> {

    Context context;

    List<Friend> friends;

    public FriendListAdapter(Context context, List<Friend> friends) {
        super(context, 0, friends);

        this.context = context;
        this.friends = friends;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Friend friend = friends.get(position);

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.friend_list_item, parent, false);
        }

        ViewHolder holder = new ViewHolder();
        holder.avatar = (ImageView) convertView.findViewById(R.id.avatar);
        holder.mobile = (ImageView) convertView.findViewById(R.id.mobile);
        holder.username = (TextView) convertView.findViewById(R.id.username);
        holder.status = (TextView) convertView.findViewById(R.id.userinfo);

        convertView.setTag(holder);

        holder.avatar.setBackgroundDrawable(null);

        Picasso.with(context).load(friend.getPic()).placeholder(R.drawable.allmystery_bg)
                .into(holder.avatar);

        holder.username.setText(friend.getUsername());
        holder.status.setText(Html.fromHtml(friend.getStatus_msg()));

        if (friend.isOnline()) {
            holder.avatar.setBackgroundDrawable(
                    getContext().getResources().getDrawable(R.drawable.imageborder));
        } else {
            holder.avatar.setBackgroundDrawable(null);
        }
        if (friend.isMobile()) {
            Picasso.with(context).load(R.drawable.mobile).into(holder.mobile);
        } else {
            holder.mobile.setImageDrawable(null);
        }

        //even odd background colors
        if (position % 2 == 0) {
            convertView.setBackgroundResource(R.drawable.listview_selector_even);
        } else {
            convertView.setBackgroundResource(R.drawable.listview_selector_odd);
        }

        return convertView;
    }

    static class ViewHolder {

        ImageView avatar;

        ImageView mobile;

        TextView username;

        TextView status;

    }


}
