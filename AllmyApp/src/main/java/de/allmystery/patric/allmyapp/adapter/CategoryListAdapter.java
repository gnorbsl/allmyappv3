package de.allmystery.patric.allmyapp.adapter;

import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.activities.CategoryActivity;
import de.allmystery.patric.allmyapp.helper.ActivityAnimator;
import de.allmystery.patric.allmyapp.helper.ThreadObject;
import de.allmystery.patric.allmyapp.helper.Tools;

/**
 * Created by Patric on 29.05.2014.
 */
public class CategoryListAdapter extends ArrayAdapter<ThreadObject> {


    Context context;

    List<ThreadObject> threads;

    boolean isBookmarks;

    public CategoryListAdapter(Context context, List<ThreadObject> threads) {
        super(context, 0, threads);

        this.context = context;
        this.threads = threads;

        isBookmarks = ((CategoryActivity) context).getCategoryString().equals("bookmarks");
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ThreadObject thread = threads.get(position);

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.thread_list_item, parent, false);
        }

        ViewHolder holder = new ViewHolder();
        holder.threadIcon = (ImageView) convertView.findViewById(R.id.threadIcon);
        holder.threadTitle = (TextView) convertView.findViewById(R.id.threadUeberschrift);
        holder.threadInfo = (TextView) convertView.findViewById(R.id.threadInfo);
        holder.watchThread = (TextView) convertView.findViewById(R.id.watchThread);
        holder.lastPost = (TextView) convertView.findViewById(R.id.lastPage);
        convertView.setTag(holder);

        Picasso.with(context).load(thread.getIcon()).into(holder.threadIcon);
        holder.threadTitle.setText(thread.getTitle());

        if (thread.isUnread()) {
            holder.threadTitle.setTypeface(null, Typeface.BOLD);
        }

        holder.threadInfo.setText(
                "letzter Beitrag " + thread.getLast_post_date() + " von " + thread
                        .getLast_post_username());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(), CategoryActivity.class);
                intent.putExtra("de.allmystery.patric.allmyapp.thread", thread);
                getContext().startActivity(intent);
                new ActivityAnimator().fadeAnimation((CategoryActivity) context);

            }
        });

        holder.lastPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(), CategoryActivity.class);
                intent.putExtra("de.allmystery.patric.allmyapp.thread", thread);
                intent.putExtra("de.allmystery.patric.allmyapp.thread.showLastPage", true);
                getContext().startActivity(intent);
                new ActivityAnimator().fadeAnimation((CategoryActivity) context);

            }
        });

        if (isBookmarks) {

            holder.watchThread.setCompoundDrawablesWithIntrinsicBounds(getContext().getResources()
                    .getIdentifier("ic_action_remove", "drawable", "de.allmystery.patric.allmyapp"),
                    0, 0, 0);
            holder.watchThread.setText("Thread nicht beobachten");

        } else {

            holder.watchThread.setText("Thread beobachten");
        }

        holder.watchThread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isBookmarks) {

                   Tools.removeThread(thread.getThread_id(), (Activity) context);

                } else {

                    Tools.addThread(thread.getThread_id(), (Activity) context);
                }

            }
        });

        //even odd background colors
        if (position % 2 == 0) {
            convertView.setBackgroundResource(R.drawable.listview_selector_even);
        } else {
            convertView.setBackgroundResource(R.drawable.listview_selector_odd);
        }

        return convertView;
    }

    static class ViewHolder {

        ImageView threadIcon;

        TextView threadTitle;

        TextView threadInfo;

        TextView watchThread;

        TextView lastPost;
    }
}
