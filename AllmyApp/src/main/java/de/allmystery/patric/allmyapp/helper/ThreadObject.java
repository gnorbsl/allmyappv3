package de.allmystery.patric.allmyapp.helper;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by freg on 14.12.2014.
 */
public class ThreadObject implements Parcelable {

    private String title;
    private int created_userid;
    private int thread_id;
    private int posts_count;
    private String last_post_date;
    private String last_post_username;
    private String created_username;
    private int pages;
    private String created_timestamp;
    private String created_date;
    private String category;
    private boolean closed;
    private String icon;
    private boolean unread;

    public String getTitle() {
        return title;
    }

    public int getCreated_userid() {
        return created_userid;
    }

    public int getThread_id() {
        return thread_id;
    }

    public int getPosts_count() {
        return posts_count;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCreated_userid(int created_userid) {
        this.created_userid = created_userid;
    }

    public void setThread_id(int thread_id) {
        this.thread_id = thread_id;
    }

    public void setPosts_count(int posts_count) {
        this.posts_count = posts_count;
    }

    public void setLast_post_date(String last_post_date) {
        this.last_post_date = last_post_date;
    }

    public void setLast_post_username(String last_post_username) {
        this.last_post_username = last_post_username;
    }

    public void setCreated_username(String created_username) {
        this.created_username = created_username;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public void setCreated_timestamp(String created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setUnread(boolean unread) {
        this.unread = unread;
    }

    public String getLast_post_date() {
        return Tools.convertTimeString(last_post_date);
    }

    public String getLast_post_username() {
        return last_post_username;
    }

    public String getCreated_username() {
        return created_username;
    }

    public int getPages() {
        return pages;
    }

    public String getCreated_timestamp() {
        return created_timestamp;
    }

    public String getCreated_date() {
        return created_date;
    }

    public String getCategory() {
        return category;
    }

    public boolean isClosed() {
        return closed;
    }

    public String getIcon() {
        return icon;
    }

    public boolean isUnread() {
        return unread;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeInt(this.created_userid);
        dest.writeInt(this.thread_id);
        dest.writeInt(this.posts_count);
        dest.writeString(this.last_post_date);
        dest.writeString(this.last_post_username);
        dest.writeString(this.created_username);
        dest.writeInt(this.pages);
        dest.writeString(this.created_timestamp);
        dest.writeString(this.created_date);
        dest.writeString(this.category);
        dest.writeByte(closed ? (byte) 1 : (byte) 0);
        dest.writeString(this.icon);
        dest.writeByte(unread ? (byte) 1 : (byte) 0);
    }

    public ThreadObject() {
    }

    private ThreadObject(Parcel in) {
        this.title = in.readString();
        this.created_userid = in.readInt();
        this.thread_id = in.readInt();
        this.posts_count = in.readInt();
        this.last_post_date = in.readString();
        this.last_post_username = in.readString();
        this.created_username = in.readString();
        this.pages = in.readInt();
        this.created_timestamp = in.readString();
        this.created_date = in.readString();
        this.category = in.readString();
        this.closed = in.readByte() != 0;
        this.icon = in.readString();
        this.unread = in.readByte() != 0;
    }

    public static final Parcelable.Creator<ThreadObject> CREATOR
            = new Parcelable.Creator<ThreadObject>() {
        public ThreadObject createFromParcel(Parcel source) {
            return new ThreadObject(source);
        }

        public ThreadObject[] newArray(int size) {
            return new ThreadObject[size];
        }
    };
}
