package de.allmystery.patric.allmyapp.events;

import android.util.Log;

/**
 * Created by freg on 22.07.2014.
 */
public class ShowLastEntryEvent {

    int pages;

    public ShowLastEntryEvent(int pages) {

        this.pages = pages;
        Log.i("LASTEVENTPAGE", "" + pages);
    }


    public int getPages() {

        return pages;
    }
}
