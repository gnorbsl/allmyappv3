package de.allmystery.patric.allmyapp.helper;

import java.util.List;

/**
 * Created by freg on 14.12.2014.
 */
public class MainResponse {

    private List<Category> categories;
    private UserData userdata;

    public List<Category> getCategories() {
        return categories;
    }

    public UserData getUserdata() {
        return userdata;
    }
}
