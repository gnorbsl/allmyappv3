package de.allmystery.patric.allmyapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.helper.AllmyConfig;
import de.allmystery.patric.allmyapp.helper.Category;

/**
 * Created by Patric on 24.05.2014.
 */
public class MainListAdapter extends ArrayAdapter<Category> {

    public MainListAdapter(Context context, List<Category> categories) {
        super(context, 0, categories);
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.category_list_item, parent, false);

            ViewHolder holder = new ViewHolder();
            holder.catImage = (ImageView) convertView.findViewById(R.id.categoryImage);
            holder.categoryTitle = (TextView) convertView.findViewById(R.id.categoryTitle);
            holder.categoryInfo = (TextView) convertView.findViewById(R.id.categoryInfo);
            holder.categoryLastThread = (TextView) convertView
                    .findViewById(R.id.categoryLastThread);
            convertView.setTag(holder);
        }

        try {

            Category category = getItem(position);

            ViewHolder holder = (ViewHolder) convertView.getTag();

            holder.catImage.setImageResource(getContext().getResources()
                    .getIdentifier(category.getCat(), "drawable",
                            "de.allmystery.patric.allmyapp"));

            holder.categoryTitle.setText(category.getTitle_long());

            holder.categoryInfo.setText(category.getLast_post_date() + " von " + category
                    .getLast_post_username());
            holder.categoryLastThread.setText(category.getLast_post_title());

            if (AllmyConfig.showSingleLine) {

                holder.categoryLastThread.setSingleLine(true);
                holder.categoryLastThread.setMaxLines(1);
            }

        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundResource(R.drawable.listview_selector_even);
        } else {
            convertView.setBackgroundResource(R.drawable.listview_selector_odd);
        }

        return convertView;
    }

    static class ViewHolder {

        ImageView catImage;

        TextView categoryTitle;

        TextView categoryInfo;

        TextView categoryLastThread;
    }


}
