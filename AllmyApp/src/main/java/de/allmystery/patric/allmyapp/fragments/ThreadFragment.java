package de.allmystery.patric.allmyapp.fragments;

import com.crashlytics.android.Crashlytics;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.adapter.CategoryPagerAdapter;
import de.allmystery.patric.allmyapp.adapter.PostListAdapter;
import de.allmystery.patric.allmyapp.events.ShowLastEntryEvent;
import de.allmystery.patric.allmyapp.helper.AllmyApiClient;
import de.allmystery.patric.allmyapp.helper.Post;
import de.allmystery.patric.allmyapp.helper.PostResponse;
import de.allmystery.patric.allmyapp.helper.ThreadObject;
import de.allmystery.patric.allmyapp.helper.Tools;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Patric on 31.05.2014.
 */
public class ThreadFragment extends ListFragment {


    static CategoryPagerAdapter categoryPagerAdapter;

    List<Post> postAdapterList;

    PostListAdapter adapter;

    ThreadObject thread;

    int threadId;

    int pageNumber;

    ListView list;

    ShowLastEntryEvent event;

    Bundle extras;

    private Menu menu;

    private ProgressInterface progressInterface;

    public static Fragment newInstance(int pageNumber, CategoryPagerAdapter categoryPagerAdapter) {

        ThreadFragment f = new ThreadFragment();
        Bundle args = new Bundle();
        //increase pagenumber if it starts att 0 it shows page 1 two times
        //TODO find a better way on clean up
        args.putInt("pageNumber", pageNumber + 1);
        f.setArguments(args);

        ThreadFragment.categoryPagerAdapter = categoryPagerAdapter;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_post, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);

        postAdapterList = new ArrayList<>();
        adapter = new PostListAdapter(getActivity(), postAdapterList);

        list = getListView();

        list.setAdapter(adapter);
        list.setCacheColorHint(Color.TRANSPARENT);

        extras = getActivity().getIntent().getExtras();
        if (extras != null && extras.containsKey("de.allmystery.patric.allmyapp.thread")) {
            thread = extras.getParcelable("de.allmystery.patric.allmyapp.thread");


        }
        pageNumber = getArguments().getInt("pageNumber");

        if (thread != null) {
            Log.i("THREAD NOT NULL", "");
            getPosts(thread.getThread_id(), false);
        } else {
            Log.i("THREADID NOT NULL", "");
            getPosts(extras.getInt("threadId"), false);
        }


    }


    private void getPosts(final int threadId, final boolean refresh) {

        progressInterface.showProgressBar(true);

        AllmyApiClient.getAllmyApiInterface(getActivity())
                .getThread(threadId, pageNumber, new Callback<PostResponse>() {
                    @Override
                    public void success(PostResponse postResponse, Response response) {

                        progressInterface.showProgressBar(false);

                        Log.i("POSTRESPONSE", postResponse != null ? "IS NOT NULL" : "IS NULL");

                        if (isAdded() && postResponse.getPosts() != null) {

                            postAdapterList.clear();
                            postAdapterList.addAll(postResponse.getPosts());

                            adapter.notifyDataSetChanged();

                            Tools.handleUserdata(postResponse.getUserdata());

                            try {

                                if (refresh) {
                                    getListView().setSelection(adapter.getCount() - 1);
                                } else {

                                    SharedPreferences threadData = getActivity()
                                            .getSharedPreferences(
                                                    String.valueOf(threadId),
                                                    Context.MODE_PRIVATE);

                                    if (threadData.contains("position")) {

                                        list.setSelection(threadData.getInt("position", 0));

                                    }

                                }

                            } catch (IllegalStateException e) {

                                Crashlytics.logException(e);


                            }

                        } else {
                            Log.i("CRASH FIXED", "YAY");
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {

                        progressInterface.showProgressBar(false);

                        if (error != null && getActivity() != null) {

                            Tools.handleError(getActivity(), error);

                        }

                    }
                });

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.threadmenu, menu);
        this.menu = menu;
        super.onCreateOptionsMenu(menu, inflater);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_watchThread:

                Tools.addThread(thread.getThread_id(), getActivity());

                return true;

            case R.id.action_refreshthread:

                refresh();

                return true;

            case R.id.menu_share:

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "http://allmystery.de/themen/" + thread.getCategory() + thread
                                .getThread_id() + (pageNumber > 1 ? "-" + (pageNumber - 1) : ""));
                sendIntent.setType("text/plain");
                startActivity(sendIntent);

                return true;

            case R.id.menu_openInBrowser:

                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://allmystery.de/themen/" + thread.getCategory() + thread
                                .getThread_id() + (pageNumber > 1 ? "-" + (pageNumber - 1) : "")));
                startActivity(browserIntent);

                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void refresh() {

        getPosts(thread.getThread_id(), true);

    }

    public void safeLastPosition() {

        SharedPreferences threadData = getActivity()
                .getSharedPreferences(String.valueOf(thread.getThread_id()),
                        Context.MODE_PRIVATE);

        SharedPreferences.Editor edit = threadData.edit();

        if (list != null) {

            edit.putInt("pageNumber", pageNumber);
            edit.putInt("position", list.getFirstVisiblePosition());

            edit.apply();

        }

    }

    public void showPosition(int position) {

        if (list != null) {
            list.setSelection(position);
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            progressInterface = (ProgressInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement Interface");
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface ProgressInterface {

        public void showProgressBar(boolean showProgressBar);


    }


}
