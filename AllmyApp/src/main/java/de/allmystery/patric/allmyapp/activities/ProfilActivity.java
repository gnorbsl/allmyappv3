package de.allmystery.patric.allmyapp.activities;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.helper.Analytics;
import de.allmystery.patric.allmyapp.helper.Tools;

/**
 * Created by freg on 18.11.2014.
 */
public class ProfilActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profil);

        Analytics.tagEvent(getApplicationContext(), "Profil angesehen");

        final WebView webView = (WebView) findViewById(R.id.profileWebView);

        Bundle extras = getIntent().getExtras();

        if (extras != null && extras.containsKey("userName")) {

            webView.loadUrl(
                    getString(R.string.apiurl) + "profiles/" + extras.getString("userName"));

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Tools.setIsVisible(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Tools.setIsVisible(true);
    }
}
