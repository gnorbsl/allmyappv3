package de.allmystery.patric.allmyapp.helper;


import android.util.Log;
import android.view.View;

public abstract class LimitedOnClickListener implements View.OnClickListener {

    private static final String TAG = LimitedOnClickListener.class.getSimpleName();

    private static final long MIN_DELAY_MS = 700;

    private static long mLastClickTime;

    @Override
    public final void onClick(View v) {
        long lastClickTime = mLastClickTime;
        long now = System.currentTimeMillis();
        mLastClickTime = now;
        if (now - lastClickTime < MIN_DELAY_MS) {
            Log.d(TAG, "click ignored");
        } else {
            // Register the click
            onSingleClick(v);
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    public abstract void onSingleClick(View v);

}