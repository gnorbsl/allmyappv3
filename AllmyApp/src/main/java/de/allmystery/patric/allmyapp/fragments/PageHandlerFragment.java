package de.allmystery.patric.allmyapp.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import de.allmystery.patric.allmyapp.R;

/**
 * Created by freg on 30.10.2014.
 */
public class PageHandlerFragment extends Fragment {

    ClickListener clickListener;

    TextView firstPage;

    TextView previousPage;

    TextView pageNumber;

    TextView nextPage;

    TextView lastPage;

    int position;

    int pages;

    public void handleNavigation(int position, int pages) {

        this.pages = pages;

        //plus 1 because viewpager starts at 0 but pages from allmystery at 1
        changePosition((position + 1));


    }

    public void changePosition(int position) {
        this.position = position;
        pageNumber.setText(this.position + " / " + this.pages);
    }

    //hides or shows navigationbuttons according to the page
    public void changePosition(int position, float positionOffset) {

        if ((position == 1 && positionOffset > 0.5) || position > 1) {

            firstPage.setVisibility(View.VISIBLE);
            previousPage.setVisibility(View.VISIBLE);
        } else {

            firstPage.setVisibility(View.INVISIBLE);
            previousPage.setVisibility(View.INVISIBLE);
        }

        if (position == (pages - 1) && positionOffset > 0.5 ||
                position == (pages) && positionOffset < 0.5) {

            nextPage.setVisibility(View.INVISIBLE);
            lastPage.setVisibility(View.INVISIBLE);
        } else {

            nextPage.setVisibility(View.VISIBLE);
            lastPage.setVisibility(View.VISIBLE);

        }

        changePosition(position);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            clickListener = (ClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ClickListener");
        }

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_page_navigation, container, false);

        firstPage = (TextView) view.findViewById(R.id.firstPage);
        previousPage = (TextView) view.findViewById(R.id.previousPage);
        pageNumber = (TextView) view.findViewById(R.id.pageNumber);
        nextPage = (TextView) view.findViewById(R.id.nextPage);
        lastPage = (TextView) view.findViewById(R.id.lastPage);

        return view;


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        firstPage.setVisibility(View.INVISIBLE);
        previousPage.setVisibility(View.INVISIBLE);
        nextPage.setVisibility(View.INVISIBLE);
        lastPage.setVisibility(View.INVISIBLE);
        pageNumber.setVisibility(View.VISIBLE);

        firstPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                changePosition(1);
                clickListener.onNavClick(position);

            }
        });

        previousPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                changePosition(position > 1 ? (position - 1) : position);
                clickListener.onNavClick(position);

            }
        });

        nextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                changePosition(position < pages ? (position + 1) : position);
                clickListener.onNavClick(position);


            }
        });

        lastPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                changePosition(pages);
                clickListener.onNavClick(position);

            }
        });

        pageNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

                alert.setTitle("Seite wählen");
                final EditText input = new EditText(getActivity());
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                alert.setView(input);

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        if (!input.getText().toString().equals("")) {

                            try {

                                int value = Integer.parseInt(input.getText().toString());
                                if (value >= 0 && value <= pages) {

                                    clickListener.onNavClick(value);

                                }
                            } catch (NumberFormatException e) {
                                dialog.cancel();
                            }


                        } else {
                            dialog.cancel();
                        }


                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.cancel();
                    }
                });

                alert.show();

            }
        });

    }

    public interface ClickListener {

        public void onNavClick(int position);
    }
}
