package de.allmystery.patric.allmyapp.helper;

import java.util.List;

/**
 * Created by freg on 16.12.2014.
 */
public class PnResponse {

    public UserData getUserdata() {
        return userdata;
    }

    public int getCount() {
        return count;
    }

    public String getChat_token() {
        return chat_token;
    }

    public boolean isIs_friend() {
        return is_friend;
    }

    private List<Message> messages;
    private UserData userdata;
    private int count;
    private String chat_token;
    private boolean is_friend;

    public List<Message> getMessages() {
        return messages;
    }
}
