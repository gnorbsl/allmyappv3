package de.allmystery.patric.allmyapp.adapter;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.activities.PnActivity;
import de.allmystery.patric.allmyapp.helper.CustomLinkMovementMethod;
import de.allmystery.patric.allmyapp.helper.Message;
import de.allmystery.patric.allmyapp.helper.Tools;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by Patric on 31.05.2014.
 */


public class PnListAdapter extends ArrayAdapter<Message> {

    Context context;

    List<Message> pns;

    ArrayList<Target> targets;

    Picasso picasso;


    public PnListAdapter(Context context, List<Message> pns) {
        super(context, 0, pns);

        this.context = context;
        this.pns = pns;
        if (picasso == null) {
            picasso = Picasso.with(context);
        }


    }

    private static Bitmap eraseBG(Bitmap src, int color) {
        int width = src.getWidth();
        int height = src.getHeight();
        Bitmap b = src.copy(Bitmap.Config.ARGB_8888, true);
        b.setHasAlpha(true);

        int[] pixels = new int[width * height];
        src.getPixels(pixels, 0, width, 0, 0, width, height);

        for (int i = 0; i < width * height; i++) {
            if (pixels[i] == color) {
                pixels[i] = 0;
            }
        }

        b.setPixels(pixels, 0, width, 0, 0, width, height);

        return b;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = null;
        convertView = null;
        ViewHolder holder;
        if (targets == null) {
            targets = new ArrayList<>();
        }

        final Message pnHelper = pns.get(position);

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);

            if (pnHelper.getUsername().equals(Tools.username)) {
                row = inflater.inflate(R.layout.pn_list_item_even, parent, false);
            } else {
                row = inflater.inflate(R.layout.pn_list_item_odd, parent, false);
            }

            holder = new ViewHolder();

            holder.avatar = (ImageView) row.findViewById(R.id.avatar);
            holder.username = (TextView) row.findViewById(R.id.username);
            holder.timeInfo = (TextView) row.findViewById(R.id.timeInfo);
            holder.pnText = (EditText) row.findViewById(R.id.text);
            row.setTag(holder);
        } else {
            row = convertView;
            holder = (ViewHolder) row.getTag();
            holder.pnText.setText("");
        }

        Picasso.with(context).load(pnHelper.getPic()).placeholder(R.drawable.allmystery_bg)
                .into(holder.avatar);

        holder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        if (pnHelper.getMessage().contains("<object width=")) {

            Pattern p = Pattern.compile("<object.*?/v/(.*?)\\?.*?object>");
            final Matcher m = p.matcher(pnHelper.getMessage());

            while (m.find()) {

                Pattern replace = Pattern.compile("<object.*?/v/" + m.group(1) + "\\?.*?object>");
                Matcher matcher = replace.matcher(pnHelper.getMessage());
                pnHelper.setMessage(matcher.replaceAll("<a href=\"vnd.youtube://" + m.group(1)
                        + "\">https://www.youtube.com/watch?v=" + m.group(1) + "</a>"));

            }
        }

        pnHelper.setMessage(pnHelper.getMessage().replaceAll("<img.*?src=\"(.*?)\".*?>",
                "<a href=\"ShowImage://$1\">Bild anzeigen</a>"));

        SpannableString pnText = new SpannableString(Html.fromHtml(pnHelper.getMessage()));

        pnText = Tools.addSmileys(context, pnText);

        holder.pnText.setText(pnText, TextView.BufferType.SPANNABLE);

        holder.pnText.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                int sdk = android.os.Build.VERSION.SDK_INT;
                if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    android.text.ClipboardManager clipboard
                            = (android.text.ClipboardManager) getContext()
                            .getSystemService(getContext().CLIPBOARD_SERVICE);
                    clipboard.setText(pnHelper.getMessage());
                    Crouton.makeText((PnActivity) getContext(), "Text in Zwischenablage kopiert",
                            Style.INFO).show();
                } else {
                    android.content.ClipboardManager clipboard
                            = (android.content.ClipboardManager) ((PnActivity) getContext())
                            .getSystemService(getContext().CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData
                            .newPlainText(pnHelper.getMessage(), pnHelper.getMessage());
                    clipboard.setPrimaryClip(clip);
                    Crouton.makeText((PnActivity) getContext(), "Text in Zwischenablage kopiert",
                            Style.INFO).show();
                }
                return true;
            }
        });

        holder.pnText.setMovementMethod(CustomLinkMovementMethod.getInstance(context));

        if (pnHelper.getUsername().equals(Tools.username)) {
            holder.username.setText("Du");
        } else {
            holder.username.setText(pnHelper.getUsername());
        }

        holder.timeInfo.setText(pnHelper.getDate());

        return row;

    }

    @Override
    public int getViewTypeCount() {

        if (pns.size() < 1) {
            return 1;
        } else {
            return pns.size();
        }

    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    static class ViewHolder {

        ImageView avatar;

        TextView username;

        TextView timeInfo;

        EditText pnText;


    }

}
