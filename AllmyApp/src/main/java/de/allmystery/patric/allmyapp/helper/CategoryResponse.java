package de.allmystery.patric.allmyapp.helper;

import java.util.List;

/**
 * Created by freg on 14.12.2014.
 */
public class CategoryResponse {

    private List<ThreadObject> threads;
    private UserData userdata;
    private int count;

    public List<ThreadObject> getThreads() {
        return threads;
    }

    public UserData getUserdata() {
        return userdata;
    }

    public int getCount() {
        return count;
    }
}
