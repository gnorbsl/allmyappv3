package de.allmystery.patric.allmyapp.adapter;

import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.allmystery.patric.allmyapp.R;
import de.allmystery.patric.allmyapp.activities.AnswerActivity;
import de.allmystery.patric.allmyapp.activities.CategoryActivity;
import de.allmystery.patric.allmyapp.activities.PnActivity;
import de.allmystery.patric.allmyapp.activities.ProfilActivity;
import de.allmystery.patric.allmyapp.helper.ActivityAnimator;
import de.allmystery.patric.allmyapp.helper.CustomLinkMovementMethod;
import de.allmystery.patric.allmyapp.helper.Post;
import de.allmystery.patric.allmyapp.helper.Tools;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by Patric on 31.05.2014.
 */


public class PostListAdapter extends ArrayAdapter<Post> {

    Context context;

    List<Post> posts;

    ListScrollListener listener;


    public PostListAdapter(Context context, List<Post> posts) {
        super(context, 0, posts);

        this.context = context;
        this.posts = posts;

        try {
            listener = (ListScrollListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement Interface");
        }


    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = null;
        ViewHolder holder;

        final Post post = posts.get(position);

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            row = inflater.inflate(R.layout.post_list_item, parent, false);

            holder = new ViewHolder();

            holder.avatar = (ImageView) row.findViewById(R.id.avatar);
            holder.username = (TextView) row.findViewById(R.id.username);
            holder.timeInfo = (TextView) row.findViewById(R.id.timeInfo);
            holder.postText = (EditText) row.findViewById(R.id.postText);

            row.setTag(holder);
        } else {
            row = convertView;
            holder = (ViewHolder) row.getTag();
            holder.postText.setText("");
        }

        Picasso.with(context).load(post.getPic()).placeholder(R.drawable.allmystery_bg)
                .into(holder.avatar);

        holder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(), AnswerActivity.class);
                intent.putExtra("de.allmystery.patric.allmyapp.thread",
                        ((CategoryActivity) getContext()).getIntent().getExtras().getParcelable(
                                "de.allmystery.patric.allmyapp.thread"));
                intent.putExtra("text", "@" + post.getUsername());
                getContext().startActivity(intent);


            }
        });

        holder.avatar.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                String names[] = {"User im Thread ansprechen", "Nachricht senden",
                        "Profil ansehen"};
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                View convertView = inflater.inflate(R.layout.custom, null);
                alertDialog.setView(convertView);
                alertDialog.setTitle("Aktion wählen");
                ListView lv = (ListView) convertView.findViewById(R.id.listView1);
                ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                        android.R.layout.simple_list_item_1, names);
                lv.setAdapter(adapter);

                final AlertDialog dialog = alertDialog.create();

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        switch (i) {
                            case 0:
                                Intent intent = new Intent(getContext(), AnswerActivity.class);
                                intent.putExtra("de.allmystery.patric.allmyapp.thread",
                                        ((CategoryActivity) getContext()).getIntent().getExtras()
                                                .getParcelable(
                                                        "de.allmystery.patric.allmyapp.thread"));
                                intent.putExtra("text", "@" + post.getUsername());
                                getContext().startActivity(intent);
                                new ActivityAnimator().fadeAnimation((Activity) getContext());
                                dialog.cancel();
                                break;
                            case 1:
                                Intent pnIntent = new Intent(context, PnActivity.class);
                                pnIntent.putExtra("username", post.getUsername());
                                pnIntent.putExtra("userId", post.getUserId());
                                context.startActivity(pnIntent);
                                new ActivityAnimator().fadeAnimation((Activity) getContext());
                                dialog.cancel();
                                break;
                            case 2:

                                Intent profilIntent = new Intent(context, ProfilActivity.class);
                                profilIntent.putExtra("userName", post.getUsername());
                                context.startActivity(profilIntent);
                                new ActivityAnimator().fadeAnimation((Activity) getContext());

                                dialog.cancel();
                                break;

                        }


                    }
                });
                dialog.show();

                return true;
            }
        });

        holder.username.setText(post.getUsername());

        if (post.getText().contains("<object width=")) {

            Pattern p = Pattern.compile("<object.*?/v/(.*?)\\?.*?object>");
            final Matcher m = p.matcher(post.getText());

            while (m.find()) {

                Pattern replace = Pattern.compile("<object.*?/v/" + m.group(1) + "\\?.*?object>");
                Matcher matcher = replace.matcher(post.getText());
                post.setText(matcher.replaceAll("<a href=\"vnd.youtube://" + m.group(1)
                        + "\">https://www.youtube.com/watch?v=" + m.group(1) + "</a>"));

            }
        }

        post.setText(post.getText().replaceAll("<img.*?src=\"(.*?)\".*?>",
                "<a href=\"ShowImage://$1\">Bild anzeigen</a>"));

        SpannableString testString = new SpannableString(Html.fromHtml(post.getText()));

        testString = Tools.addSmileys(context, testString);

        holder.postText.setText(testString, TextView.BufferType.SPANNABLE);
        holder.postText.invalidate();

        holder.postText.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                int sdk = android.os.Build.VERSION.SDK_INT;
                if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    android.text.ClipboardManager clipboard
                            = (android.text.ClipboardManager) getContext()
                            .getSystemService(Context.CLIPBOARD_SERVICE);
                    clipboard.setText(post.getText());
                    Crouton.makeText((CategoryActivity) getContext(),
                            "Text in Zwischenablage kopiert", Style.INFO).show();
                } else {
                    android.content.ClipboardManager clipboard
                            = (android.content.ClipboardManager) ((CategoryActivity) getContext())
                            .getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData
                            .newPlainText(post.getText(), post.getText());
                    clipboard.setPrimaryClip(clip);
                    Crouton.makeText((CategoryActivity) getContext(),
                            "Text in Zwischenablage kopiert", Style.INFO).show();
                }
                return true;
            }
        });

        try {
            holder.username.setTextColor(Color.parseColor(post.getUsercolor()));
        } catch (IllegalArgumentException e) {
            holder.username.setTextColor(Color.WHITE);
        }

        holder.timeInfo.setText(post.getDate());

        //even odd background colors
        if (position % 2 == 0) {
            row.setBackgroundResource(R.drawable.listview_selector_even);
        } else {
            row.setBackgroundResource(R.drawable.listview_selector_odd);
        }

        holder.postText.setMovementMethod(CustomLinkMovementMethod.getInstance(context));

        return row;

    }


    @Override
    public int getViewTypeCount() {

        if (posts.size() < 1) {
            return 1;
        } else {
            return posts.size();
        }

    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    public interface ListScrollListener {

        public void showTitle();
    }

    static class ViewHolder {

        ImageView avatar;

        TextView username;

        TextView timeInfo;

        EditText postText;
    }

}
